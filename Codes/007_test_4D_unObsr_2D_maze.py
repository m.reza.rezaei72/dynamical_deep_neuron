''' load the dataset
data spec: MSTrain:27305x67 (time resolution 33ms)
col[0] : time index
col[1-62] : spiking activities
col[63] : x-position
col[64] : y-position
col[65] : x-velocity
col[66] : y-velocity
'''
from utils import *
from D4_regularized import D4_regularized
import numpy as np
import matplotlib.pyplot as plt
Data=sio.loadmat('../Dataset/Data33.mat')['MSTrain']
num_folds=1
perf_metrics_4D_x=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_4D_y=np.zeros((num_folds,3,2))
perf_metrics_4D_xy=np.zeros((num_folds,3,2))

'''preprocess the data; finding informative channels of spikes and
 smoothing them with a gaussian window'''

hist_l = 20
NeuralData=Data[:,1:63]
bestIndxs=calInformetiveChan(NeuralData,100)
config_LSSM_MPP = {
        'X_bounds': [-2, 2],  # define lower and upper bound of state estimation
        'X_length': 100,  # the whole state bound quantized tp X_length values for numerical calculations
        'number_MCS':100,  # number of trajectory samples for expectation calculations in EM step
        'opt_method': 'Simple-sampling',  # optimization method for expectation calculation
        'epoches': 10, # maximum number of iterations for fminsearch the parameters optimization
        'maxIter':400, # maximum number of iterations for the loop of parameters optimization
        'lam_reg': -.1,
        'training_mode': 'un_supervised' # 'supervised' or 'un_supervised'

    }
states=np.linspace(config_LSSM_MPP['X_bounds'][0],config_LSSM_MPP['X_bounds'][1], config_LSSM_MPP['X_length'])
states = states.reshape([-1, 1])

YCells=calSmoothNeuralActivity(np.squeeze(NeuralData[:,bestIndxs]),10,4)
# normalization
for iii in range(YCells.shape[-1]):
    YCells[:,iii]=(YCells[:,iii]-YCells[:,iii].mean())/YCells[:,iii].std()
# scale betwen the state limites
for iii in range(YCells.shape[-1]):
    YCells[:,iii]=((YCells[:,iii]-YCells[:,iii].min())/(YCells[:,iii].max()-YCells[:,iii].min())
                   -0.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])

Data[:,63]=((Data[:,63]-np.min(Data[:,63]))/(np.max(Data[:,63])-np.min(Data[:,63]))-.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])
Data[:,64]=((Data[:,64]-np.min(Data[:,64]))/(np.max(Data[:,64])-np.min(Data[:,64]))-.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])

(YCells_tr,YCells_te,xPos_tr,xPos_te,yPos_tr,yPos_te,t_tr,t_te)=train_test_split(YCells, Data[:,63],Data[:,64],Data[:,0], test_size=.2,random_state=0,shuffle=False) # split test and train daata
XSgn_tr_y=calDesignMatrix_V2(YCells_tr,hist_l)
XSgn_te_y=calDesignMatrix_V2(YCells_te,hist_l)


# Train Y


SSM_Y = D4_regularized(config_LSSM_MPP, observ=XSgn_tr_y, target=yPos_tr, states=states)
SSM_Y.parameters_opt_EM(epoches=config_LSSM_MPP['epoches'], observ=XSgn_tr_y,target=yPos_tr,
                          maxiter=config_LSSM_MPP['maxIter'])
[Sigma_XX_Y,A_xx_Y,B_xx_Y,mu_X0_Y,sigma_X0_Y]= SSM_Y.get_model_parms()

[y_mu_smooth_tr, y_var_smooth_tr]=model_output_visualiztion_1D(SSM_Y,config_LSSM_MPP,XSgn_tr_y,yPos_tr,'train_result_Y' )
# Test Y
[y_mu_smooth_te, y_var_smooth_te]=model_output_visualiztion_1D(SSM_Y,config_LSSM_MPP,XSgn_te_y,yPos_te,'test_result_Y' )
perf_metrics_4D_y[0,:,:]=cal_performance_4D(y_mu_smooth_tr.squeeze(),y_var_smooth_tr.squeeze(),yPos_tr.squeeze(),
                                            y_mu_smooth_te.squeeze(),y_var_smooth_te.squeeze(),yPos_te.squeeze())

DDDD_result_y = vis_performance(perf_metrics_4D_y, '4D_y')


# Train X
XSgn_tr_x=XSgn_tr_y#np.concatenate([XSgn_tr_y,calDesignMatrix_V2(y_mu_smooth_tr,hist_l)],axis=-1)
XSgn_te_x=XSgn_te_y#np.concatenate([XSgn_te_y,calDesignMatrix_V2(y_mu_smooth_te,hist_l)],axis=-1)
SSM_x = D4_regularized(config_LSSM_MPP, observ=XSgn_tr_x, target=xPos_tr, states=states)
SSM_x.parameters_opt_EM(epoches=config_LSSM_MPP['epoches'], observ=XSgn_tr_x,target=xPos_tr,
                          maxiter=config_LSSM_MPP['maxIter'])
[Sigma_XX,A_xx,B_xx,mu_X0,sigma_X0]= SSM_x.get_model_parms()
[x_mu_smooth_tr, x_var_smooth_tr]=model_output_visualiztion_1D(SSM_x,config_LSSM_MPP,XSgn_tr_x,xPos_tr,'train_result_X' )
# Test X
[x_mu_smooth_te, x_var_smooth_te]=model_output_visualiztion_1D(SSM_x,config_LSSM_MPP,XSgn_te_x,xPos_te,'test_result_X' )
perf_metrics_4D_x[0,:,:]=cal_performance_4D(x_mu_smooth_tr.squeeze(),x_var_smooth_tr.squeeze(),xPos_tr.squeeze(),
                                            x_mu_smooth_te.squeeze(),x_var_smooth_te.squeeze(),xPos_te.squeeze())

DDDD_result_x = vis_performance(perf_metrics_4D_x, '4D_x')

