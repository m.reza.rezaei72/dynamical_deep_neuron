from D4_regularized import D4_regularized
from Data_Generator_V2 import Data_Generator_V2,cov_gen
import numpy as np
from scipy.stats import pearsonr
from utils import *
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold
number_LFP_chs = 20
num_folds=2
kf = KFold(n_splits=num_folds, random_state=None, shuffle=False)

perf_metrics_4D=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_DNN=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_SSM=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
'''  '''
config_synthetic_Data = {  ## set parameters fpr  x state
        'alphaX': 1,  # state transition Coff
        'sigmaX': .1,  # noise variance
        'initial_X': 0,  # state initialization
        'lim_X': [-1, 1],  # bounds for possible values for the state x
        'h_K':5,
        ## parameters for the LFP signals generation
        'number_LFPs': number_LFP_chs,
        'LFPs_sigma': cov_gen(.2,.5,number_LFP_chs,10),
        'LFPs_init': np.zeros([number_LFP_chs, 1]).squeeze(),
        'LFPs_alpha': np.ones( (number_LFP_chs,)),
        ## simulation time
        'dt': 0.001,
        'simulation_duration': 1,
}
config_LSSM_MPP = {
        'X_bounds': [-2, 2],  # define lower and upper bound of state estimation
        'X_length': 400,  # the whole state bound quantized tp X_length values for numerical calculations
        'number_MCS':100,  # number of trajectory samples for expectation calculations in EM step
        'opt_method': 'Simple-sampling',  # optimization method for expectation calculation
        'epoches': 5, # maximum number of iterations for fminsearch the parameters optimization
        'maxIter':400, # maximum number of iterations for the loop of parameters optimization
        'lam_reg': 0.0,
        'training_mode': 'un_supervised' # 'supervised' or 'un_supervised'

    }
# define the states
states=np.linspace(config_LSSM_MPP['X_bounds'][0],config_LSSM_MPP['X_bounds'][1], config_LSSM_MPP['X_length'])
states = states.reshape([-1, 1])

Synth_gen = Data_Generator_V2(config_synthetic_Data)
X = Synth_gen.generate_state_fun()
X=((X-np.min(X))/(np.max(X)-np.min(X))-.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])
Qs = []
klhs = []
Qsklh = []
# lam_reng = np.exp(np.linspace(np.log(.1), np.log(3), 10))
lam_reng = (np.linspace(0,1, 10))
# lam_reng = [-0.65]
abss_corr = np.zeros((len(lam_reng),num_folds))
mses = np.zeros((len(lam_reng),num_folds))
lam_ind=-1
for lam in lam_reng:
    lam_ind+=1
    Neural_Features_d = Synth_gen.generate_LFPs_obs_fun()
    config_LSSM_MPP['lam_reg'] = lam
    Neural_Features_d=calDesignMatrix_V2(Neural_Features_d,10)
    fold_indx=-1

    Q = 0
    klh = 0
    for train_index, test_index in kf.split(X):
        fold_indx+=1
        if fold_indx<1:
            XSgn_tr=Neural_Features_d[train_index]
            XSgn_te = Neural_Features_d[test_index]
            X_tr=X[train_index]
            X_te = X[test_index]

            #
            SSM = D4_regularized(config_LSSM_MPP, observ=XSgn_tr, target=X_tr, states=states)
            Q=SSM.parameters_opt_EM(epoches=config_LSSM_MPP['epoches'], observ=XSgn_tr,target=X_tr,
                                      maxiter=config_LSSM_MPP['maxIter'])
            klh = np.sum(SSM.get_klh())
            klhs.append(klh)
            [Sigma_XX, A_xx, B_xx, mu_X0, sigma_X0] = SSM.get_model_parms()
            [x_mu_smooth_tr, x_var_smooth_tr] = model_output_visualiztion_1D(SSM, config_LSSM_MPP, XSgn_tr, X_tr,
                                                                             'train_result_X_fold-'+str(fold_indx))
            # Test X
            [x_mu_smooth_te, x_var_smooth_te] = model_output_visualiztion_1D(SSM, config_LSSM_MPP, XSgn_te, X_te,
                                                                             'test_result_X_fold-'+str(fold_indx))

            perf_metrics_4D[fold_indx,:,:]= cal_performance_4D(x_mu_smooth_tr,x_var_smooth_tr,X_tr,x_mu_smooth_te,x_var_smooth_te,X_te)
            # print(np.abs(pearsonr(x_mu_smooth_te.squeeze(),X_te.squeeze())[0]))
            abss_corr[lam_ind,fold_indx] = np.abs(pearsonr(x_mu_smooth_te.squeeze(), X_te.squeeze())[0])
            if  (pearsonr(x_mu_smooth_te.squeeze(), X_te.squeeze())[0])<0:
                mses[lam_ind, fold_indx] = np.sqrt(np.mean((-x_mu_smooth_te.squeeze()- X_te.squeeze())**2))
            else:
                mses[lam_ind, fold_indx] = np.sqrt(np.mean((x_mu_smooth_te.squeeze() - X_te.squeeze()) ** 2))

            Qs.append(Q)
            Qsklh.append(Q + klh)

fig, axs = plt.subplots(2, 2)

axs[0, 0].plot(lam_reng,Qs)
axs[0, 0].set_title('Qs-regularized')
axs[0, 1].plot(lam_reng,klhs)
axs[0, 1].set_title('KL-H')
axs[1, 0].plot(lam_reng,Qsklh)
axs[1, 0].set_title('Qsklh')
axs[1, 1].scatter (mses[:,0], abss_corr[:,0])
axs[1, 1].set_xlabel ('mse')
axs[1, 1].set_ylabel ('corr. coef.')
for kk in range(len(lam_reng)):
    axs[1, 1].annotate(str(round(lam_reng[kk],2)), (mses[kk,0], abss_corr[kk,0]))
axs[1, 1].set_title('mse and abss_corr')
