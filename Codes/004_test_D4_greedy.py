from D4_greedy import D4_greedy
from Data_Generator_V2 import Data_Generator_V2 ,cov_gen
import numpy as np
from utils import *
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold
from scipy.stats import pearsonr
number_LFP_chs = 20
num_folds=2
kf = KFold(n_splits=num_folds, random_state=None, shuffle=False)

perf_metrics_4D=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_DNN=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_SSM=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
'''  '''
config_synthetic_Data = {  ## set parameters fpr  x state
        'alphaX': 1,  # state transition Coff
        'sigmaX': .1,  # noise variance
        'initial_X': 0,  # state initialization
        'lim_X': [-2, 2],  # bounds for possible values for the state x
        'h_K':5,

        ## parameters for the LFP signals generation
        'number_LFPs': number_LFP_chs,
        'LFPs_sigma': cov_gen(.2,.4,number_LFP_chs,100),
        'LFPs_init': np.zeros([number_LFP_chs, 1]).squeeze(),
        'LFPs_alpha': np.ones( (number_LFP_chs,)),
        ## simulation time
        'dt': 0.001,
        'simulation_duration': 1,
}
config_LSSM_MPP = {
        'X_bounds': [-2, 2],  # define lower and upper bound of state estimation
        'X_length': 400,  # the whole state bound quantized tp X_length values for numerical calculations
        'number_MCS':100,  # number of trajectory samples for expectation calculations in EM step
        'opt_method': 'Simple-sampling',  # optimization method for expectation calculation
        'epoches': 20, # maximum number of iterations for fminsearch the parameters optimization
        'maxIter':15, # maximum number of iterations for the loop of parameters optimization
        'training_mode': 'supervised' # 'supervised' or 'un_supervised'

    }
# define the states
states=np.linspace(config_LSSM_MPP['X_bounds'][0],config_LSSM_MPP['X_bounds'][1], config_LSSM_MPP['X_length'])
states = states.reshape([-1, 1])

Synth_gen = Data_Generator_V2(config_synthetic_Data)
X = Synth_gen.generate_state_fun()
X=((X-np.min(X))/(np.max(X)-np.min(X))-.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])

Neural_Features_d_org = Synth_gen.generate_LFPs_obs_fun()
Qs=[]
klhs=[]
Qsklh = []
abss_corr =[]
for hist_length in range(1,10):
    Neural_Features_d=calDesignMatrix_V2(Neural_Features_d_org,hist_length)
    fold_indx=-1
    Q=0
    klh=0

    for train_index, test_index in kf.split(X):

        fold_indx+=1
        if fold_indx <1:

            XSgn_tr=Neural_Features_d[train_index]
            XSgn_te = Neural_Features_d[test_index]
            X_tr=X[train_index]
            X_te = X[test_index]

            #
            SSM = D4_greedy(config_LSSM_MPP, observ=XSgn_tr, target=X_tr, states=states)
            Q=SSM.parameters_opt_EM(epoches=config_LSSM_MPP['epoches'], observ=XSgn_tr,target=X_tr,
                                      maxiter=config_LSSM_MPP['maxIter'])
            klh=np.sum( SSM.get_klh())
            klhs.append(klh)
            [Sigma_XX, A_xx, B_xx, mu_X0, sigma_X0] = SSM.get_model_parms()
            [x_mu_smooth_tr, x_var_smooth_tr] = model_output_visualiztion_1D(SSM, config_LSSM_MPP, XSgn_tr, X_tr,
                                                                             'train_result_X_fold-'+str(fold_indx))
            # Test X
            [x_mu_smooth_te, x_var_smooth_te] = model_output_visualiztion_1D(SSM, config_LSSM_MPP, XSgn_te, X_te,
                                                                             'test_result_X_fold-'+str(fold_indx))
            abss_corr.append(np.abs(pearsonr(x_mu_smooth_te.squeeze(),X_te.squeeze())[0]))
            # perf_metrics_4D[fold_indx,:,:]= cal_performance_4D(x_mu_smooth_tr,x_var_smooth_tr,X_tr,x_mu_smooth_te,x_var_smooth_te,X_te)
            #
            # DNN_model=baseline_DNNs(XSgn_tr, X_tr)
            # perf_metrics_DNN[fold_indx,:,:]=cal_baseline_DNNs_performance(DNN_model, XSgn_tr, X_tr, XSgn_te, X_te,config_LSSM_MPP)
            #
            # GLM_model=baseline_GLMs(XSgn_tr[:,0,:].squeeze(), X_tr)
            # perf_metrics_SSM[fold_indx, :, :] = cal_baseline_GLMs_performance(GLM_model, XSgn_tr[:,0,:].squeeze(), X_tr, XSgn_te[:,0,:].squeeze(), X_te,config_LSSM_MPP)
    Qs.append(Q)
    Qsklh.append(Q-klh)

fig, axs = plt.subplots(2,2)

axs[0,0].plot(Qs)
axs[0,0].set_title('Qs-KL')

axs[0,1].plot(klhs)
axs[0,1].set_title('KL-H')

axs[1,0].plot(Qsklh)
axs[1,0].set_title('Qsklh')

axs[1,1].plot(abss_corr)
axs[1,1].set_title('abss_corr')

# DDDD_result = vis_performance(perf_metrics_4D, '4D')
# DNN_result = vis_performance(perf_metrics_DNN, 'DNN')
# GLM_result = vis_performance(perf_metrics_SSM, 'GLM')
#
# tot_result= pd.DataFrame()
# tot_result[DDDD_result.columns] = DDDD_result
# tot_result[DNN_result.columns] = DNN_result
# tot_result[GLM_result.columns] = GLM_result
# plt.figure()
# tot_result[['4D_MAE_tr','4D_MAE_te','DNN_MAE_tr','DNN_MAE_te', 'GLM_MAE_tr', 'GLM_MAE_te']].boxplot()
# plt.xticks(rotation = 90)
#
# plt.figure()
# tot_result[['4D_RMSE_tr','4D_RMSE_te','DNN_RMSE_tr','DNN_RMSE_te', 'GLM_RMSE_tr', 'GLM_RMSE_te']].boxplot()
# plt.xticks(rotation = 90)
#
# plt.figure()
# tot_result[['4D_95%-HPD_tr','4D_95%-HPD_te','DNN_95%-HPD_tr','DNN_95%-HPD_te', 'GLM_95%-HPD_tr', 'GLM_95%-HPD_tr']].boxplot()
# plt.xticks(rotation = 90)