import numpy as np
import scipy.io as sio
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from scipy.linalg import hankel
from sklearn.metrics import accuracy_score, f1_score
import tensorflow as tf
import tensorflow_probability as tfp
import statsmodels.api as sm
from tensorflow.keras.initializers import RandomUniform
tfd = tfp.distributions
tfk = tf.keras
tfkl = tf.keras.layers
tfpl = tfp.layers
tfd = tfp.distributions
from keras.optimizers import adam_v2
import keras.backend as K
import tensorflow_probability as tfp
tfd = tfp.distributions
import statsmodels.api as sm
from scipy.stats import norm
import pandas as pd
import scipy.optimize
########## R-squared (R2) ##########

def get_R2(y_test,y_test_pred):

    """
    Function to get R2

    Parameters
    ----------
    y_test - the true outputs (a matrix of size number of examples x number of outputs)
    y_test_pred - the predicted outputs (a matrix of size number of examples x number of outputs)

    Returns
    -------
    R2_array: An array of R2s for each output
    """

  #Compute R2 for each output
    y_mean=np.mean(y_test)
    R2=1-np.sum((y_test_pred-y_test)**2)/np.sum((y_test-y_mean)**2)

    return R2 #Return an array of R2s




########## Pearson's correlation (rho) ##########
def get_rmse(y_test,y_test_pred):
    rmse=np.sqrt(np.mean((y_test-y_test_pred)**2))

    return rmse


def get_mae_2D(X_true,Y_true, X_pred,Y_pred):
    mae = np.sqrt(np.mean(np.abs(X_true - X_pred)))

    return mae
def get_mae(y_test, y_test_pred):
    mae = np.sqrt(np.mean(np.abs(y_test - y_test_pred)))

    return mae

def get_rho(y_test,y_test_pred):

    """
    Function to get Pearson's correlation (rho)

    Parameters
    ----------
    y_test - the true outputs (a matrix of size number of examples x number of outputs)
    y_test_pred - the predicted outputs (a matrix of size number of examples x number of outputs)

    Returns
    -------
    rho_array: An array of rho's for each output
    """


    rho=np.corrcoef(y_test.T,y_test_pred.T)[0,1]

    return rho #Return the array of rhos

def calInformetiveChan(data,minNumSpiks):

    return np.where(np.sum(data,axis=0)>minNumSpiks)

def calSmoothNeuralActivity(data,gausWindowLength,gausWindowSigma):
    x=np.linspace(-1*gausWindowSigma,1*gausWindowSigma,gausWindowLength)
    gausWindow=1/(2*np.pi*gausWindowSigma)*np.exp(-0.5*(x**2/gausWindowSigma**2))
    gausWindow=gausWindow/np.max(gausWindow)
    #plt.plot(x,gausWindow)
    #plt.show()
    dataSmooth=np.zeros(data.shape)
    for i in range(data.shape[1]):
        dataSmooth[:,i]=np.convolve(data[:,i],gausWindow,'same')
        #dataSmooth[np.where(dataSmooth[:,i] <0), i]=0
    #plt.subplot(2,1,1)
    #plt.plot(data[:5000,1])
    #plt.subplot(2, 1, 2)
    #plt.plot(dataSmooth[:5000, 1])
    #plt.show()
    return dataSmooth

def calDesignMatrix(X,h):
    PadX = np.zeros([h , X.shape[1]])
    PadX =np.concatenate([PadX,X],axis=0)
    XDsgn=np.zeros([X.shape[0], X.shape[1]*h])
    for i in range(X.shape[1]):
         XDsgn[:, i* h : (i+1) * h]= hankel(PadX[:  - h , i],PadX[ - h :, i])
    return XDsgn

def model_output_visualiztion_1D(SSM,config_LSSM_MPP,Neural_Features_d,X,title ):

    [Pxx, Plx, P, Psmooth_c, x_mu, x_var,
         x_mu_smooth, x_var_smooth, x_vals, Qs] = SSM.get_resutl(observ=Neural_Features_d)




    xs_id = np.arange(len(x_mu))

    # f, axes = plt.subplots(1, 1, sharex=False, sharey=False)
    # axes.plot(np.arange(config_LSSM_MPP['epoches']), Qs, 'r')
    f, axes = plt.subplots(2, 1, sharex=True, sharey=True)
    axes[0].fill_between(xs_id, (x_mu_smooth - 2 * np.sqrt(x_var_smooth)).squeeze(), (x_mu_smooth + 2 * np.sqrt(x_var_smooth)).squeeze(), color='r',
                             label='HI-DGD 95%', alpha=.3)
    axes[0].plot(xs_id, x_mu_smooth, 'r', label='Dynamical-VAE conf')
    axes[0].plot(xs_id, X, 'k', label='True')
    axes[0].legend()


    axes[1].fill_between(xs_id,
                             (Plx.dot(x_vals) - 2 * np.sqrt(Plx.dot(x_vals ** 2) - (Plx.dot(x_vals)) ** 2)).squeeze(),
                             (Plx.dot(x_vals) + 2 * np.sqrt(Plx.dot(x_vals ** 2) - (Plx.dot(x_vals)) ** 2)).squeeze(),
                             color='r', label='marginal neural 95%', alpha=.3)
    axes[1].plot(xs_id, Plx.dot(x_vals), 'r', label='marginal neural conf')
    axes[1].plot(xs_id, X, 'k', label='True')

    axes[1].set_ylim([config_LSSM_MPP['X_bounds'][0], config_LSSM_MPP['X_bounds'][1]])
    axes[1].legend()
    axes[1].set_title(title)
    return x_mu_smooth, x_var_smooth

def calDesignMatrix_V2(X,h):
    '''

    :param X: [samples*Feature]
    :param h: hist
    :return: [samples*hist*Feature]

    '''
    PadX = np.zeros([h , X.shape[1]])
    PadX =np.concatenate([PadX,X],axis=0)
    XDsgn=np.zeros([X.shape[0], h, X.shape[1]])
    # print(PadX.shapepe)
    for i in range(0,XDsgn.shape[0]):
         #print(i)
         XDsgn[i, : , :]= (PadX[i:h+i,:])
    return XDsgn

def cal_performance_4D(x_mu_tr,x_var_tr,X_tr,
                       x_mu_te,x_var_te,X_te):
    perf_metric=np.zeros((3,2))
    # MAE
    perf_metric[0,0]=get_mae(X_tr, x_mu_tr)
    perf_metric[0, 1] =get_mae(X_te, x_mu_te)

    # MSE
    perf_metric[1, 0] = get_rmse(X_tr, x_mu_tr)
    perf_metric[1, 1] = get_rmse(X_te, x_mu_te)

    # 95% hpd
    perf_metric[2, 0] =cal_hpd95(x_mu_tr,x_var_tr,X_tr)
    perf_metric[2, 1] = cal_hpd95(x_mu_te,x_var_te,X_te)
    return perf_metric


def cal_hpd95(mu, var,true):
    hpd=0
    for i in range(len(mu)):
        if (true[i]<= mu[i]+2*np.sqrt(var[i])) and (true[i]>= mu[i]-2*np.sqrt(var[i])):
            hpd+=1
    return hpd/len(mu)

def vis_performance(perf_metrics,title):

    Df=pd.DataFrame()
    Df[title+'_'+'MAE_tr']=perf_metrics[:,0,0]
    Df[title+'_'+'MAE_te'] = perf_metrics[:, 0, 1]

    Df[title+'_'+'RMSE_tr'] = perf_metrics[:, 1, 0]
    Df[title+'_'+'RMSE_te'] = perf_metrics[:, 1, 1]

    Df[title+'_'+'95%-HPD_tr'] = perf_metrics[:, 2, 0]
    Df[title+'_'+'95%-HPD_te'] = perf_metrics[:, 2, 1]
    plt.figure()
    Df.boxplot()
    # plt.ylim([0,1])
    plt.title(title)
    plt.xticks(rotation=90)
    Df.describe()
    return Df

def baseline_DNNs(observ,target ):
    batch_size = 2000
    prediction_model = tf.keras.Sequential([
    tf.keras.layers.InputLayer(input_shape=[observ.shape[1], observ.shape[2]]),
        # tf.keras.layers.GRU(10, activation='tanh', dropout=.2, recurrent_dropout=.2, return_sequences=True),
        tf.keras.layers.GRU(5, activation='tanh', dropout=.2, recurrent_dropout=.2, return_sequences=False),
        tf.keras.layers.Dense(5, activation=None, kernel_initializer='glorot_uniform', bias_initializer='zeros'),
        tf.keras.layers.Dense(1, activation=None, kernel_initializer='glorot_uniform', bias_initializer='zeros'),

    ])
    ''' Variational Auto encoder'''
    prediction_model.compile(loss='mse', optimizer=adam_v2.Adam(learning_rate=0.001),
                                      metrics=['mse'])
    prediction_model.fit(observ, target, batch_size=batch_size, epochs=600, verbose=0)
    prediction_model.summary()
    return prediction_model

def cal_baseline_DNNs_performance(model,XSgn_tr, X_tr, XSgn_te, X_te,config_LSSM_MPP):
    x_mu_tr=model.predict(XSgn_tr).squeeze()
    x_mu_te = model.predict(XSgn_te).squeeze()

    perf_metric = np.zeros((3, 2))
    # MAE
    perf_metric[0, 0] = get_mae(X_tr ,x_mu_tr)
    perf_metric[0, 1] = get_mae(X_te, x_mu_te)

    # MSE
    perf_metric[1, 0] = get_rmse(X_tr, x_mu_tr)
    perf_metric[1, 1] = get_rmse(X_te, x_mu_te)

    # 95% hpd
    # perf_metric[2, 0] = cal_hpd95(x_mu_tr, x_var_tr, X_tr)
    # perf_metric[2, 1] = cal_hpd95(x_mu_te, x_var_te, X_te)

    f, axes = plt.subplots(2, 1, sharex=False, sharey=True)

    xs_id = np.arange(len(X_tr))
    axes[0].plot(xs_id, x_mu_tr, 'r', label='DNN Train')
    axes[0].plot(xs_id, X_tr, 'k', label='True')
    axes[0].set_ylim([config_LSSM_MPP['X_bounds'][0], config_LSSM_MPP['X_bounds'][1]])
    axes[0].legend()

    xs_id = np.arange(len(X_te))
    axes[1].plot(xs_id, x_mu_te, 'r', label='DNN test')
    axes[1].plot(xs_id, X_te, 'k', label='True')
    axes[1].set_ylim([config_LSSM_MPP['X_bounds'][0], config_LSSM_MPP['X_bounds'][1]])
    axes[1].legend()

    return perf_metric

def baseline_GLMs(observ,target ):
    glm_model = sm.GLM(target, observ, family=sm.families.Gaussian())
    glm_results = glm_model.fit()
    return glm_results


def cal_baseline_GLMs_performance(model,XSgn_tr, X_tr, XSgn_te, X_te,config_LSSM_MPP):
    x_mu_tr=model.predict(XSgn_tr)
    x_var_tr=((model.scale)**2 * np.ones(X_tr.shape)).squeeze()
    x_mu_te = model.predict(XSgn_te)
    x_var_te = ((model.scale)**2 * np.ones(X_te.shape)).squeeze()

    perf_metric = np.zeros((3, 2))
    # MAE
    perf_metric[0, 0] = get_mae(X_tr, x_mu_tr)
    perf_metric[0, 1] = get_mae(X_te, x_mu_te)

    # MSE
    perf_metric[1, 0] = get_rmse(X_tr, x_mu_tr)
    perf_metric[1, 1] = get_rmse(X_te, x_mu_te)

    # 95% hpd
    perf_metric[2, 0] = cal_hpd95(x_mu_tr,x_var_tr , X_tr.squeeze())
    perf_metric[2, 1] = cal_hpd95(x_mu_te, x_var_te, X_te.squeeze())

    f, axes = plt.subplots(2, 1, sharex=False, sharey=True)

    xs_id = np.arange(len(X_tr))
    axes[0].fill_between(xs_id,
                         (x_mu_tr - 2 * np.sqrt(x_var_tr)).squeeze(),
                         (x_mu_tr + 2 * np.sqrt(x_var_tr)).squeeze(),
                         color='r', label='marginal neural 95%', alpha=.3)
    axes[0].plot(xs_id, x_mu_tr, 'r', label='GLM Train')
    axes[0].plot(xs_id, X_tr, 'k', label='True')
    axes[0].set_ylim([config_LSSM_MPP['X_bounds'][0], config_LSSM_MPP['X_bounds'][1]])
    axes[0].legend()


    xs_id = np.arange(len(X_te))
    axes[1].fill_between(xs_id,
                         (x_mu_te - 2 * np.sqrt(x_var_te)).squeeze(),
                         (x_mu_te + 2 * np.sqrt(x_var_te)).squeeze(),
                         color='r', label='marginal neural 95%', alpha=.3)
    axes[1].plot(xs_id, x_mu_te, 'r', label='GLM test')
    axes[1].plot(xs_id, X_te, 'k', label='True')
    axes[1].set_ylim([config_LSSM_MPP['X_bounds'][0], config_LSSM_MPP['X_bounds'][1]])
    axes[1].legend()

    return perf_metric


def cal_baseline_GLMs_performance_2D(model_x, XSgn_tr_x, X_tr,
                                                                XSgn_te_x, X_te,
                                                                            model_y, XSgn_tr_y,
                                                                            Y_tr,
                                                                            XSgn_te_y,
                                                                            Y_te,
                                                                            config_LSSM_MPP):
    x_mu_tr = model_x.predict(XSgn_tr_x)
    x_var_tr = ((model_x.scale) ** 2 * np.ones(X_tr.shape)).squeeze()
    x_mu_te = model_x.predict(XSgn_te_x)
    x_var_te = ((model_x.scale) ** 2 * np.ones(X_te.shape)).squeeze()

    y_mu_tr = model_y.predict(XSgn_tr_y)
    y_var_tr = ((model_y.scale) ** 2 * np.ones(Y_tr.shape)).squeeze()
    y_mu_te = model_y.predict(XSgn_te_y)
    y_var_te = ((model_y.scale) ** 2 * np.ones(Y_te.shape)).squeeze()
    perf_metric = np.zeros((3, 2))
    # MAE
    perf_metric[0, 0] = get_mae_2D(X_tr,Y_tr, x_mu_tr,y_mu_tr)
    perf_metric[0, 1] = get_mae_2D(X_te,Y_te, x_mu_te,y_mu_te)

    # MSE
    perf_metric[1, 0] = get_rmse_2D(X_tr,Y_tr, x_mu_tr,y_mu_tr)
    perf_metric[1, 1] = get_rmse_2D(X_te,Y_te, x_mu_te,y_mu_te)

    # 95% hpd
    perf_metric[2, 0] = cal_hpd95_2D(x_mu_tr, x_var_tr, X_tr.squeeze(),
                                     y_mu_tr, y_var_tr, Y_tr.squeeze())

    perf_metric[2, 1] = cal_hpd95_2D(x_mu_te, x_var_te, X_te.squeeze(),
                                     y_mu_te, y_var_te, Y_te.squeeze())