import numpy as np
from scipy import interpolate
from scipy.stats import norm
import scipy
import math
Eps = np.exp(-10)
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.keras.initializers import RandomUniform
tfd = tfp.distributions
tfk = tf.keras
tfkl = tf.keras.layers
tfpl = tfp.layers
tfd = tfp.distributions
from keras.optimizers import adam_v2
import keras.backend as K
from scipy.special import kl_div, rel_entr
from scipy.stats import entropy
np.random.seed(seed=123)
import matplotlib.pyplot as plt
import tqdm
# negloglik = lambda y, p_y: -p_y.log_prob(y) #+1*tf.math.reduce_sum( tf.keras.losses.MSE(y,p_y))
def neg_log_likelihood(y_true, y_pred):
    # loss = 0
    # for i in range(y_true.shape[-1]):
    #     loss+=K.sum(K.sum(-y_pred.log_prob(y_true[:,i])))

    return K.sum(-y_pred.log_prob(y_true))#+tf.math.reduce_sum( tf.keras.losses.MSE(y_pred,y_true))
# Specify the surrogate posterior over `keras.layers.Dense` `kernel` and `bias`.
def posterior_mean_field(kernel_size, bias_size=0, dtype=None):
            n = kernel_size + bias_size
            c = np.log(np.expm1(1.))
            return tf.keras.Sequential([
                tfp.layers.VariableLayer(2 * n, dtype=dtype),
                tfp.layers.DistributionLambda(lambda t: tfd.Independent(
                    tfd.Normal(loc=t[..., :n],
                               scale=1e-5 + tf.nn.softplus(c + t[..., n:])),
                    reinterpreted_batch_ndims=1)),
            ])

# Specify the prior over `keras.layers.Dense` `kernel` and `bias`.
def prior_trainable(kernel_size, bias_size=0, dtype=None):
            n = kernel_size + bias_size
            return tf.keras.Sequential([
                tfp.layers.VariableLayer(n, dtype=dtype),
                tfp.layers.DistributionLambda(lambda t: tfd.Independent(
                    tfd.Normal(loc=t, scale=1),
                    reinterpreted_batch_ndims=1)),
            ])


class D4_greedy(object):
    def __init__(self, config, observ, target, states):
        '''
        :param config: the model configuration
        '''

        self.X_bounds = config['X_bounds']
        self.X_length = config['X_length']
        self.number_MCS = config['number_MCS'] ## number of samples for posterior calculation
        self.opt_method = config['opt_method']
        self.training_mode = config['training_mode']


        self.X = states #

        self.params_random_init( observ,target)
        self.get_hist(observ)



    def state_transition_cal(self):
        '''
        define  a linear state transition p(x_k|x_(k-1))=N(A x_(k-1), signa_x)
        :return: state transition model
        '''

        self.Pxx = np.ones((self.X_length, self.X_length)) / (self.X_length * self.X_length)
        covered_regions=np.where(self.hist_obsr_X !=0)[0]
        compn_w=np.zeros((self.hist_obsr_X.shape))
        compn_w[covered_regions]=1/(self.hist_obsr_X[covered_regions])
        # compn_w/=compn_w.sum()
        for i in range(0, self.X_length):
            m = self.X[i].dot(self.A_xx) + self.B_xx
            self.Pxx[i, :] = norm.pdf(self.X.squeeze(), m, self.Sigma_XX)#*compn_w.transpose()
            self.Pxx[i, :] = self.Pxx[i, :] / self.Pxx[i, :].sum()
        return self.Pxx


    def params_random_init(self, observ,target):


        ''' x0 '''
        self.mu_X0 = 0 * (self.X_bounds[1] + self.X_bounds[0]) * np.ones((1, 1))
        self.sigma_X0 = .5* (self.X_bounds[1] - self.X_bounds[0]) * np.ones(
            (1, 1))  # (self.X_bounds[1]-self.X_bounds[0])* np.ones((1,1))/4
        ''' xk|x_(k-1) '''
        self.A_xx =1* np.ones((1, 1))  # state transition coeff.
        self.B_xx = 0 * np.ones((1, 1))  # state transition coeff.
        self.Sigma_XX = .2 * np.ones((1, 1))
        ''' reaction time observation '''
        ### to be completed
        initializer = RandomUniform(-1, 1)
        config = initializer.get_config()
        initializer = RandomUniform.from_config(config)
        self.batch_size = 2000
        self.prediction_model = tf.keras.Sequential([
            tf.keras.layers.InputLayer(input_shape=[observ.shape[1],observ.shape[2]]),
            tf.keras.layers.Flatten(),
            # tf.keras.layers.GRU(10, activation='tanh', dropout=.2, recurrent_dropout=.2, return_sequences=True),
            # tf.keras.layers.GRU(50, activation='tanh', dropout=.0, recurrent_dropout=.0, return_sequences=False),
            # tf.keras.layers.Dense(5, activation=None, kernel_initializer='glorot_uniform', bias_initializer='zeros'),
            # tf.keras.layers.Dense(5,activation=None,kernel_initializer='glorot_uniform',bias_initializer='zeros'),
            tf.keras.layers.Dense(2, activation=None,kernel_initializer='glorot_uniform',bias_initializer='zeros'),

            tfp.layers.DistributionLambda(
                    lambda t: tfd.Normal(loc=t[..., :1],
                                         scale=.1+ tf.nn.relu(1* t[..., 1:]))),

        ])
        ''' Variational Auto encoder'''



        if self.training_mode=='un_supervised':
            self.prediction_model.compile(loss=neg_log_likelihood, optimizer=adam_v2.Adam(learning_rate=0.001), metrics=['mse'])
            # self.prediction_model.compile(loss='mse', optimizer=Adam(learning_rate=0.001), metrics=['mse'])
            # self.prediction_model.fit(observ,observ.mean(axis=1), batch_size=self.batch_size, epochs=1, verbose=0)
            self.prediction_model.summary()
        elif self.training_mode=='supervised':
            self.prediction_model.compile(loss=neg_log_likelihood, optimizer=adam_v2.Adam(learning_rate=0.001), metrics=['mse'])
            # self.prediction_model.compile(loss='mse', optimizer=Adam(learning_rate=0.001), metrics=['mse'])
            self.prediction_model.fit(observ,target, batch_size=self.batch_size, epochs=600, verbose=0)
            self.prediction_model.summary()
    def observation_process(self, observ):
        '''
        define discriminative observation process with a (I approximated it with Gaussian process)
        :param N:  LFPs
        :param Thr_on: if True means
        :return:
        '''

        self.Plx = np.ones((observ.shape[0], self.X_length)) / self.X_length
        self.Plx_hk = np.ones((observ.shape[0], self.X_length)) / self.X_length
        xmu = self.prediction_model(observ)

        # cal p(xk|hk)
        histo=observ.copy()
        histo[:,-1,:]=0
        xmu_histo = self.prediction_model(histo)
        for i in range((observ.shape[0])):

            self.Plx[i, :] = norm.pdf(self.X, xmu.mean()[i], xmu.stddev()[i]).transpose()
            self.Plx_hk[i, :] = norm.pdf(self.X, xmu_histo.mean()[i], xmu_histo.stddev()[i]).transpose()

            self.Plx[i, :] = (self.Plx[i, :]+Eps) /(self.Plx[i, :].sum()+Eps)
            self.Plx_hk[i, :] = ( self.Plx_hk[i, :]+Eps)/(self.Plx_hk[i, :].sum()+Eps)


        return self.Plx


    def posterior_cal(self):
        '''
        calculate the Posterior density function from the observation processes
        :return: posterior density function for all time points
        '''
        T, L = np.shape(self.Plx)
        self.P = np.zeros((T, L))
        self.P_one_step = np.zeros((T, L))
        self.X_hat_m = np.zeros((T, 1))
        self.X_hat_s = np.zeros((T, 1))

        Plf_bfr = np.ones((L,)) / L # initiate the PDF
        for i in range(0, T):
            if i == 0:
                self.P[0, :] = norm.pdf(self.X.squeeze(), self.mu_X0, self.sigma_X0)
                Plf_bfr = self.P[0, :]
            else:
                self.P_one_step[i,:] = self.Pxx.dot(Plf_bfr)
                pint = self.Pxx.dot(self.P[i - 1, :])

                self.P[i, :] = pint * ( (self.Plx[i, :]+Eps)/(self.P_one_step[i,:]+Eps))
                # self.P[i, :] = pint *  (self.Plx[i, :] )
                Plf_bfr = self.Plx[i, :]
                self.P[i, :] = self.P[i, :] / (self.P[i, :].sum())
            self.X_hat_m[i] = self.P[i, :].dot(self.X).squeeze()
            self.X_hat_s[i] = self.P[i, :].dot(self.X ** 2).squeeze() - self.X_hat_m[i] ** 2


    def posterior_smoother(self):
        '''
        calculate smoothed version of the Posterior density function
        :return:
        '''
        T, L = np.shape(self.Plx)
        self.Psmooth = np.zeros((T, L))

        self.X_hat_m_smooth = np.zeros((T, 1))
        self.X_hat_s_smooth = np.zeros((T, 1))
        for i in range(0, T):
            dd = T - 1 - i
            if dd == T - 1:
                self.Psmooth[dd, :] = self.P[dd, :]  # set an initial value for P_smooth[:,K], last observation
            else:
                # I used the formula I wrote in the paper as the smoother
                self.Psmooth[dd, :] = self.P[dd, :] * (self.Pxx).dot(
                    (self.Psmooth[dd + 1, :] + Eps) / (self.Pxx.dot(self.P[dd, :]) + Eps))
                self.Psmooth[dd, :] = self.Psmooth[dd, :] / self.Psmooth[dd, :].sum()
            self.X_hat_m_smooth[dd] = self.Psmooth[dd, :].dot(self.X)
            self.X_hat_s_smooth[dd] = (self.Psmooth[dd, :]).dot(self.X ** 2) - self.X_hat_m_smooth[dd] ** 2

    def smoother(self, P):
        '''
        calculate smoothed version of the input pdf for all times
        :param P: input PDF
        :return: smoothed PDF for p
        '''
        T, L = np.shape(P)
        P_s = np.zeros((T, L))
        for i in range(0, T):
            dd = T - 1 - i
            if dd == T - 1:
                P_s[dd, :] = P[dd, :]  # set an initial value for P_smooth[:,K], last observation
            else:
                # I used the formula I wrote in the paper as the smoother
                P_s[dd, :] = P[dd, :] * (self.Pxx).dot(
                    (P_s[dd + 1, :] + Eps) / (self.Pxx.dot(P[dd, :]) + Eps))
                P_s[dd, :] = P_s[dd, :] / P_s[dd, :].sum()
        return P_s

    def parameters_opt_EM(self, epoches, observ,target, maxiter):
        self.Qs=[]
        for itr in range(epoches):
            # at epoch 0 initial the parameters values
            print('epoches=%d'%itr)
            if itr == 0:
                # self.initial_model(observ,target)
                self.Q_val_prev = 1000000000000
                self.expectation(observ, skip_obsr=False)
            else:
                # do expectation step in EM and state process


                if self.training_mode == 'un_supervised':
                    self.expectation(observ,skip_obsr=True)
                    self.MCMC_sampling(observ)
                    self.prediction_model.compile(loss=neg_log_likelihood, optimizer=adam_v2.Adam(learning_rate=0.001), metrics=['mse'])
                    self.prediction_model.fit(observ, self.X_samples, batch_size=self.batch_size, epochs=600, verbose=0)
                elif self.training_mode=='supervised':
                    pass
                self.expectation(observ, skip_obsr=False)
                '''sampling'''
                self.MCMC_sampling(observ)
                # plt.figure()
                # plt.plot(self.X_samples)
                # do maximization step in EM and state process
                Q = self.state_param_max(maxiter)
                Q+=self.get_EM_smoother_pred()
                self.Qs.append(Q)

        return Q


    def get_EM_smoother_pred(self):
        em = 0
        for i in range(self.Psmooth.shape[0]):
            em += np.sum(self.Psmooth[i, :] * np.log((self.Plx[i, :] + Eps)))

        return em / self.Psmooth.shape[0]

    def get_klh(self):
        klh=0
        for i in range(self.Psmooth.shape[0]):
            klh+= np.sum(kl_div(self.Psmooth[i,:]+Eps,self.Plx_hk[i,:]+Eps))+np.sum(entropy(self.Psmooth[i,:]+Eps))


        return klh/self.Psmooth.shape[0]


    def MCMC_sampler(self,P):
        samples= np.zeros((P.shape[0], self.number_MCS))
        P_sampels = np.zeros([P.shape[0], self.X_length])
        for d in range(P.shape[0]):
            i = P.shape[0] - d - 1
            if i == P.shape[0] - 1:
                P_sampels[i, :] = P[i, :]
            else:
                P_sampels[i, :] = (self.Pxx.dot(P[i, :]) + Eps)

                P_sampels[i, :] = P_sampels[i, :] / P_sampels[i, :].sum()

            r = np.random.rand(self.number_MCS)
            cdf_y = np.cumsum(P_sampels[i, :])  # cumulative distribution function, cdf
            inv_cdf = interpolate.interp1d(cdf_y.squeeze(), self.X.squeeze(), bounds_error=False)
            samples[i, :] = inv_cdf(r)
        for ii in range(self.number_MCS):
            box = np.ones(10)
            samples[:, ii] = np.convolve(samples[:, ii], box, mode='same')
        return samples

    def MCMC_sampling(self, observ):
        self.X_samples = self.MCMC_sampler(self.Psmooth)

        if self.training_mode == 'un_supervised':

            self.X_samples[np.isnan(self.X_samples)] = 0
            self.X_samples = ((self.X_samples - np.min(np.min(self.X_samples))) / (
                    np.max(np.max(self.X_samples)) - np.min(np.min(self.X_samples))) - 0.5) * np.abs(
            self.X_bounds[1] - self.X_bounds[0])

    def state_param_max(self, maxiter):

        # take Numb_Samples samples from the posterior (without considering importance sampling, just MCS)

        # self.X_samples = self.X_hat_m_smooth
        # concatonate all parameters of the SMM from previous epoch as
        # initial values for current epoch for fmin_search Opt.

        Ini_Par = np.concatenate([self.Sigma_XX.reshape([-1, 1]),
                                  self.A_xx.reshape([-1, 1]),
                                  self.B_xx.reshape([-1, 1]),
                                  self.mu_X0.reshape([-1, 1]),
                                  self.sigma_X0.reshape([-1, 1])],
                                 axis=0).squeeze()
        # do parameters estimation

        (temp, Q_val, iter, funcalls, warnflg) = scipy.optimize.fmin(func=ExpectedQ, x0=Ini_Par
                                                                     , args=(self.X, self.X_samples),
                                                                     maxiter=maxiter,
                                                                     full_output=True,
                                                                     disp=False)
        # self.Q_val_prev > Q_val and (temp[0]>0.05)
        if  self.Q_val_prev > Q_val    :

            self.Q_val_prev = Q_val
            print(-1*Q_val)
            # replace the model's parameters with new ones
            if ~math.isnan(temp[0]):
                self.Sigma_XX = np.abs(temp[0])
            if ~math.isnan(temp[1]):
                self.A_xx = temp[1]
            if ~math.isnan(temp[2]):
                self.B_xx = temp[2]
            if ~math.isnan(temp[3]):
                self.mu_X0 = temp[3]
            if ~math.isnan(temp[4]):
                self.sigma_X0 = np.abs(temp[4])

        return -1*self.Q_val_prev
    #
    def expectation(self, observ, skip_obsr=False):
        '''
        do expectation step in EM
        :param S: reaction time Obs.

        :param N: LFP Obs.
        :return:
        '''
        self.state_transition_cal()
        if skip_obsr:
            pass
        else:
            self.observation_process(observ)
        self.posterior_cal()
        self.posterior_smoother()


    def get_resutl(self, observ):
        '''
         get the DGD model result based on the updated parameters
        :param S: reaction time Obs.

        :param N: LFP Obs.
        :return:
        '''
        self.state_transition_cal()
        self.observation_process(observ)

        self.posterior_cal()
        self.posterior_smoother()
        self.P = self.smoother(self.P)

        self.Plx= self.smoother(self.Plx)
        return [self.Pxx, self.Plx, self.P, self.Psmooth, self.X_hat_m, self.X_hat_s,
                self.X_hat_m_smooth, self.X_hat_s_smooth, self.X,self.Qs]

    def get_model_parms(self):
        '''
        :return: return the discriminative part parameters
        '''
        return self.Sigma_XX.squeeze(),\
               self.A_xx.squeeze(),\
               self.B_xx.squeeze(),\
               self.mu_X0.squeeze(),\
               self.sigma_X0.squeeze(),\

    def get_hist(self, obsr):

        self.hist_obsr_X=np.zeros((self.X.shape))
        for i in range(self.X.shape[0]-1):
            self.hist_obsr_X[i]=len(np.where((obsr >= self.X[i])&(obsr < self.X[i+1]))[0])



def ExpectedQ(par, X, X_samples ):

    # Pxx parameters
    sigmae =par[0]
    A_xx = par[1]
    B_xx = par[2]
    m0 = par[3]
    sigma0 = par[4]

    E0 = 0
    Exx = 0
    # EXS = 0
    ###########################################################
    temp_E0 = -0.5 * np.log(sigma0 ** 2) - 0.5 * (
            (X_samples[0, :] - m0) ** 2) / sigma0 ** 2
    E0 += np.nansum(np.nansum(temp_E0))/X_samples.shape[1]
   ###########################################################

    m = X_samples[1:-1, :] - (A_xx * X_samples[2:, :] + B_xx)
    Temp_Exx = -0.5 * np.log(sigmae ** 2) - 0.5 * (
            (m) ** 2) / sigmae ** 2
    Exx += np.nansum(np.nansum(Temp_Exx)) / (X_samples.shape[0] - 2)/X_samples.shape[1]

    # for i in range(X_samples.shape[1]):
    # m = X_samples_pred[1:-1, :] - (A_xx * X_samples_onestep[2:, i:i + 1] + B_xx)
    # Temp_Exs = -0.5 * np.log(sigmae ** 2) - 0.5 * (
    #             (m) ** 2) / sigmae ** 2
    #     EXS += np.nansum(np.nansum(Temp_Exs)) / (X_samples.shape[0] - 2)# / (X_samples.shape[1])



    LoweLimitQ = -1 * ( Exx +E0)

    return LoweLimitQ


