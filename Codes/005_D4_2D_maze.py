''' load the dataset
data spec: MSTrain:27305x67 (time resolution 33ms)
col[0] : time index
col[1-62] : spiking activities
col[63] : x-position
col[64] : y-position
col[65] : x-velocity
col[66] : y-velocity
'''
from utils import *
from D4_regularized import D4_regularized
import numpy as np
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
Data=sio.loadmat('../Dataset/Data33.mat')['MSTrain']
num_folds=5
kf = KFold(n_splits=num_folds, random_state=None, shuffle=False)
perf_metrics_4D_x=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_4D_y=np.zeros((num_folds,3,2))
perf_metrics_4D_xy=np.zeros((num_folds,3,2))

perf_metrics_GLM_x=np.zeros((num_folds,3,2))
perf_metrics_GLM_y=np.zeros((num_folds,3,2))
perf_metrics_GLM_xy=np.zeros((num_folds,3,2))

perf_metrics_DNN_x=np.zeros((num_folds,3,2))
perf_metrics_DNN_y=np.zeros((num_folds,3,2))
perf_metrics_DNN_xy=np.zeros((num_folds,3,2))
'''preprocess the data; finding informative channels of spikes and
 smoothing them with a gaussian window'''

hist_l = 20
NeuralData=Data[:,1:63]
bestIndxs=calInformetiveChan(NeuralData,100)
config_LSSM_MPP = {
        'X_bounds': [-2, 2],  # define lower and upper bound of state estimation
        'X_length': 100,  # the whole state bound quantized tp X_length values for numerical calculations
        'number_MCS':10,  # number of trajectory samples for expectation calculations in EM step
        'opt_method': 'Simple-sampling',  # optimization method for expectation calculation
        'epoches': 10, # maximum number of iterations for fminsearch the parameters optimization
        'maxIter':10, # maximum number of iterations for the loop of parameters optimization
        'lam_reg':-.8,
        'training_mode': 'supervised' # 'supervised' or 'un_supervised'

    }
states=np.linspace(config_LSSM_MPP['X_bounds'][0],config_LSSM_MPP['X_bounds'][1], config_LSSM_MPP['X_length'])
states = states.reshape([-1, 1])

YCells=calSmoothNeuralActivity(np.squeeze(NeuralData[:,bestIndxs]),10,4)
# normalization
for iii in range(YCells.shape[-1]):
    YCells[:,iii]=(YCells[:,iii]-YCells[:,iii].mean())/YCells[:,iii].std()
# scale betwen the state limites
for iii in range(YCells.shape[-1]):
    YCells[:,iii]=((YCells[:,iii]-YCells[:,iii].min())/(YCells[:,iii].max()-YCells[:,iii].min())
                   -0.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])

Data[:,63]=((Data[:,63]-np.min(Data[:,63]))/(np.max(Data[:,63])-np.min(Data[:,63]))-.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])
Data[:,64]=((Data[:,64]-np.min(Data[:,64]))/(np.max(Data[:,64])-np.min(Data[:,64]))-.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])
fold_indx=-1
for train_index, test_index in kf.split(Data[:,63]):
    fold_indx+=1


    YCells_tr=YCells[train_index]
    YCells_te = YCells[test_index]

    xPos_tr=Data[train_index,63]
    xPos_te = Data[test_index, 63]

    yPos_tr = Data[train_index, 64]
    yPos_te = Data[test_index, 64]

    t_tr = Data[train_index, 0]
    t_te = Data[test_index, 0]


    XSgn_tr_y=calDesignMatrix_V2(YCells_tr,hist_l)
    XSgn_te_y=calDesignMatrix_V2(YCells_te,hist_l)


    # Train Y


    SSM_Y = D4_regularized(config_LSSM_MPP, observ=XSgn_tr_y, target=yPos_tr, states=states)
    SSM_Y.parameters_opt_EM(epoches=config_LSSM_MPP['epoches'], observ=XSgn_tr_y,target=yPos_tr,
                              maxiter=config_LSSM_MPP['maxIter'])
    [Sigma_XX_Y,A_xx_Y,B_xx_Y,mu_X0_Y,sigma_X0_Y]= SSM_Y.get_model_parms()

    [y_mu_smooth_tr, y_var_smooth_tr]=model_output_visualiztion_1D(SSM_Y,config_LSSM_MPP,XSgn_tr_y,yPos_tr,'train_result_Y' )
    # Test Y
    [y_mu_smooth_te, y_var_smooth_te]=model_output_visualiztion_1D(SSM_Y,config_LSSM_MPP,XSgn_te_y,yPos_te,'test_result_Y' )
    perf_metrics_4D_y[fold_indx,:,:]=cal_performance_4D(y_mu_smooth_tr.squeeze(),y_var_smooth_tr.squeeze(),yPos_tr.squeeze(),
                                                y_mu_smooth_te.squeeze(),y_var_smooth_te.squeeze(),yPos_te.squeeze())

    DNN_model_y=baseline_DNNs(XSgn_tr_y, yPos_tr)
    perf_metrics_DNN_y[fold_indx,:,:]=cal_baseline_DNNs_performance(DNN_model_y, XSgn_tr_y, yPos_tr.squeeze(),
                                                            XSgn_te_y, yPos_te.squeeze(),config_LSSM_MPP)

    GLM_model_y=baseline_GLMs(XSgn_tr_y[:,0,:].squeeze(), yPos_tr)
    perf_metrics_GLM_y[fold_indx, :, :] = cal_baseline_GLMs_performance(GLM_model_y, XSgn_tr_y[:,0,:].squeeze(), yPos_tr.squeeze(),
                                                                XSgn_te_y[:,0,:].squeeze(), yPos_te.squeeze(),config_LSSM_MPP)







    # Train X
    XSgn_tr_x=np.concatenate([XSgn_tr_y,calDesignMatrix_V2(y_mu_smooth_tr,hist_l)],axis=-1)
    XSgn_te_x=np.concatenate([XSgn_te_y,calDesignMatrix_V2(y_mu_smooth_te,hist_l)],axis=-1)
    SSM_x = D4_regularized(config_LSSM_MPP, observ=XSgn_tr_x, target=xPos_tr, states=states)
    SSM_x.parameters_opt_EM(epoches=config_LSSM_MPP['epoches'], observ=XSgn_tr_x,target=xPos_tr,
                              maxiter=config_LSSM_MPP['maxIter'])
    [Sigma_XX,A_xx,B_xx,mu_X0,sigma_X0]= SSM_x.get_model_parms()
    [x_mu_smooth_tr, x_var_smooth_tr]=model_output_visualiztion_1D(SSM_x,config_LSSM_MPP,XSgn_tr_x,xPos_tr,'train_result_X' )
    # Test X
    [x_mu_smooth_te, x_var_smooth_te]=model_output_visualiztion_1D(SSM_x,config_LSSM_MPP,XSgn_te_x,xPos_te,'test_result_X' )
    perf_metrics_4D_x[fold_indx,:,:]=cal_performance_4D(x_mu_smooth_tr.squeeze(),x_var_smooth_tr.squeeze(),xPos_tr.squeeze(),
                                                x_mu_smooth_te.squeeze(),x_var_smooth_te.squeeze(),xPos_te.squeeze())


    DNN_model_x=baseline_DNNs(XSgn_tr_x, xPos_tr)
    perf_metrics_DNN_x[fold_indx,:,:]=cal_baseline_DNNs_performance(DNN_model_x, XSgn_tr_x, xPos_tr.squeeze(), XSgn_te_x,
                                                            xPos_te.squeeze(),config_LSSM_MPP)

    GLM_model_x=baseline_GLMs(XSgn_tr_x[:,0,:].squeeze(), xPos_tr)
    perf_metrics_GLM_x[fold_indx, :, :] = cal_baseline_GLMs_performance(GLM_model_x, XSgn_tr_x[:,0,:].squeeze(), xPos_tr.squeeze(),
                                                                XSgn_te_x[:,0,:].squeeze(), xPos_te.squeeze(),config_LSSM_MPP)

    # perf_metrics_GLM_xy[fold_indx, :, :] = cal_baseline_GLMs_performance_2D(GLM_model_x, XSgn_tr_x[:,0,:].squeeze(), xPos_tr.squeeze(),
    #                                                             XSgn_te_x[:,0,:].squeeze(), xPos_te.squeeze(),
    #                                                                         GLM_model_y, XSgn_tr_y[:, 0, :].squeeze(),
    #                                                                         yPos_tr.squeeze(),
    #                                                                         XSgn_te_y[:, 0, :].squeeze(),
    #                                                                         yPos_te.squeeze(),
    #                                                                         config_LSSM_MPP)


## 2D performance
# perf_metrics_4D_xy[0,:,:]=cal_performance_4D_2D(x_mu_smooth_tr,x_var_smooth_tr,xPos_tr,x_mu_smooth_te,x_var_smooth_te,xPos_te,
#                                              y_mu_smooth_tr,y_var_smooth_tr,yPos_tr,y_mu_smooth_te,y_var_smooth_te,yPos_te)
DDDD_result_y = vis_performance(perf_metrics_4D_y, '4D_y')
DNN_result_y = vis_performance(perf_metrics_DNN_y, 'DNN_y')
GLM_result_y = vis_performance(perf_metrics_GLM_y, 'GLM_y')
DDDD_result_x = vis_performance(perf_metrics_4D_x, '4D_x')
DNN_result_x = vis_performance(perf_metrics_DNN_x, 'DNN_x')
GLM_result_x = vis_performance(perf_metrics_GLM_x, 'GLM_x')
tot_result = pd.concat([DDDD_result_x,DDDD_result_y,DNN_result_x,DNN_result_y,GLM_result_x,GLM_result_y])

tot_result= pd.DataFrame()
tot_result[DDDD_result_x.columns] = DDDD_result_x
tot_result[DDDD_result_y.columns] = DDDD_result_y
tot_result[DNN_result_x.columns] = DNN_result_x
tot_result[DNN_result_y.columns] = DNN_result_y
tot_result[GLM_result_x.columns] = GLM_result_x
tot_result[GLM_result_y.columns] = GLM_result_y

plt.figure()
tot_result[['4D_x_MAE_tr', '4D_y_MAE_tr',
            '4D_x_MAE_te','4D_y_MAE_te',
            'DNN_x_MAE_tr', 'DNN_y_MAE_tr',
            'DNN_x_MAE_te','DNN_y_MAE_te',
            'GLM_x_MAE_tr','GLM_y_MAE_tr',
            'GLM_x_MAE_te','GLM_y_MAE_te']].boxplot()
plt.xticks(rotation = 90)

plt.figure()
tot_result[['4D_x_RMSE_tr','4D_y_RMSE_tr',
            '4D_x_RMSE_te','4D_y_RMSE_te',
            'DNN_x_RMSE_tr','DNN_y_RMSE_tr',
            'DNN_x_RMSE_te', 'DNN_y_RMSE_te',
            'GLM_x_RMSE_tr','GLM_y_RMSE_tr' ,
            'GLM_x_RMSE_te','GLM_y_RMSE_te']].boxplot()
plt.xticks(rotation = 90)

plt.figure()
tot_result[['4D_x_95%-HPD_tr','4D_y_95%-HPD_tr',
            '4D_x_95%-HPD_te', '4D_y_95%-HPD_te',
            'DNN_x_95%-HPD_tr', 'DNN_y_95%-HPD_tr',
            'DNN_x_95%-HPD_te', 'DNN_y_95%-HPD_te',
            'GLM_x_95%-HPD_tr', 'GLM_y_95%-HPD_tr',
            'GLM_x_95%-HPD_tr', 'GLM_y_95%-HPD_tr']].boxplot()
plt.xticks(rotation = 90)