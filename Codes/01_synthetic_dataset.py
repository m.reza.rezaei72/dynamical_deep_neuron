from synthetic_dataset_utils import *
import pandas as pd
import seaborn as sns
import numpy as np


# generate some clusters
cluster1 = GMM_sample_gen(
    torch.Tensor([2.5, 2.5]),
    torch.Tensor([1.2, .8]),
    nb_samples=500
)

cluster2 = GMM_sample_gen(
    torch.Tensor([7.5, 7.5]),
    torch.Tensor([.75, .5]),
    nb_samples=500
)

cluster3 = GMM_sample_gen(
    torch.Tensor([8, 1.5]),
    torch.Tensor([.6, .8]),
    nb_samples=1000
)

X = torch.cat([cluster1, cluster2, cluster3])
plot_2d_sample(X)