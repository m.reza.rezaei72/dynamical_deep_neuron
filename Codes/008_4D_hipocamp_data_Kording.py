''' load the dataset
data spec: MSTrain:27305x67 (time resolution 33ms)
col[0] : time index
col[1-62] : spiking activities
col[63] : x-position
col[64] : y-position
col[65] : x-velocity
col[66] : y-velocity
'''
from utils import *
from D4_greedy import Dynamic_VAE
import numpy as np
import matplotlib.pyplot as plt
Data=pd.read_pickle('../Dataset/example_data_hc.pickle')
subrange=np.arange(1000,5000)
num_folds=1
perf_metrics_4D_x=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_4D_y=np.zeros((num_folds,3,2))
perf_metrics_4D_xy=np.zeros((num_folds,3,2))

perf_metrics_GLM_x=np.zeros((num_folds,3,2))
perf_metrics_GLM_y=np.zeros((num_folds,3,2))
perf_metrics_GLM_xy=np.zeros((num_folds,3,2))

perf_metrics_DNN_x=np.zeros((num_folds,3,2))
perf_metrics_DNN_y=np.zeros((num_folds,3,2))
perf_metrics_DNN_xy=np.zeros((num_folds,3,2))
'''preprocess the data; finding informative channels of spikes and
 smoothing them with a gaussian window'''

hist_l = 10
NeuralData=Data[0][subrange,:]
bestIndxs=calInformetiveChan(NeuralData,10)
config_LSSM_MPP = {
        'X_bounds': [0, 1],  # define lower and upper bound of state estimation
        'X_length': 100,  # the whole state bound quantized tp X_length values for numerical calculations
        'number_MCS':50,  # number of trajectory samples for expectation calculations in EM step
        'opt_method': 'Simple-sampling',  # optimization method for expectation calculation
        'epoches': 15, # maximum number of iterations for fminsearch the parameters optimization
        'maxIter':250, # maximum number of iterations for the loop of parameters optimization
        'training_mode': 'supervised' # 'supervised' or 'un_supervised'

    }
states=np.linspace(config_LSSM_MPP['X_bounds'][0],config_LSSM_MPP['X_bounds'][1], config_LSSM_MPP['X_length'])
states = states.reshape([-1, 1])

YCells=calSmoothNeuralActivity(np.squeeze(NeuralData[:,bestIndxs]),3,1)
# normalization
for iii in range(YCells.shape[-1]):
    YCells[:,iii]=(YCells[:,iii]-YCells[:,iii].mean())/YCells[:,iii].std()
# scale betwen the state limites
for iii in range(YCells.shape[-1]):
    YCells[:,iii]=((YCells[:,iii]-YCells[:,iii].min())/(YCells[:,iii].max()-YCells[:,iii].min())
                   )*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])

Xpos = Data[1][subrange,0]
Xpos[np.isnan(Xpos)] = 0
Ypos = Data[1][subrange,1]
Ypos[np.isnan(Ypos)] = 0
Xpos=((Xpos-np.min(Xpos))/(np.max(Xpos)-np.min(Xpos)))*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])
Ypos=((Ypos-np.min(Ypos))/(np.max(Ypos)-np.min(Ypos)))*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])

(YCells_tr,YCells_te,xPos_tr,xPos_te,yPos_tr,yPos_te,t_tr,t_te)=train_test_split(YCells, Xpos,Ypos,np.arange(len(Xpos)),
                                                                                 test_size=.5,random_state=0,shuffle=False) # split test and train daata

Qs=[]
for hist_length in range(1,20):
    Q=0
    XSgn_tr_y=calDesignMatrix_V2(YCells_tr,hist_l)
    XSgn_te_y=calDesignMatrix_V2(YCells_te,hist_l)


    # Train Y


    SSM_Y = Dynamic_VAE(config_LSSM_MPP, observ=XSgn_tr_y, target=yPos_tr, states=states)
    Q=SSM_Y.parameters_opt_EM(epoches=config_LSSM_MPP['epoches'], observ=XSgn_tr_y,target=yPos_tr,
                              maxiter=config_LSSM_MPP['maxIter'])
    Qs.append(-1*Q)
    [Sigma_XX_Y,A_xx_Y,B_xx_Y,mu_X0_Y,sigma_X0_Y]= SSM_Y.get_model_parms()

    [y_mu_smooth_tr, y_var_smooth_tr]=model_output_visualiztion_1D(SSM_Y,config_LSSM_MPP,XSgn_tr_y,yPos_tr,'train_result_Y' )
plt.figure()
plt.plot(Qs/Qs[0])
plt.show()
# Test Y
# [y_mu_smooth_te, y_var_smooth_te]=model_output_visualiztion_1D(SSM_Y,config_LSSM_MPP,XSgn_te_y,yPos_te,'test_result_Y' )
# perf_metrics_4D_y[0,:,:]=cal_performance_4D(y_mu_smooth_tr.squeeze(),y_var_smooth_tr.squeeze(),yPos_tr.squeeze(),
#                                             y_mu_smooth_te.squeeze(),y_var_smooth_te.squeeze(),yPos_te.squeeze())
#
# DNN_model_y=baseline_DNNs(XSgn_tr_y, yPos_tr)
# perf_metrics_DNN_y[0,:,:]=cal_baseline_DNNs_performance(DNN_model_y, XSgn_tr_y, yPos_tr.squeeze(), XSgn_te_y, yPos_te.squeeze(),config_LSSM_MPP)
#
# GLM_model_y=baseline_GLMs(XSgn_tr_y[:,0,:].squeeze(), yPos_tr)
# perf_metrics_GLM_y[0, :, :] = cal_baseline_GLMs_performance(GLM_model_y, XSgn_tr_y[:,0,:].squeeze(), yPos_tr,
#                                                             XSgn_te_y[:,0,:].squeeze(), yPos_te,config_LSSM_MPP)

# DDDD_result_y = vis_performance(perf_metrics_4D_y, '4D_y')
# DNN_result_y = vis_performance(perf_metrics_DNN_y, 'DNN_y')
# GLM_result_y = vis_performance(perf_metrics_GLM_y, 'GLM_y')
#
#
#
# # Train X
# XSgn_tr_x=XSgn_tr_y#np.concatenate([XSgn_tr_y,calDesignMatrix_V2(y_mu_smooth_tr,hist_l)],axis=-1)
# XSgn_te_x=XSgn_te_y#np.concatenate([XSgn_te_y,calDesignMatrix_V2(y_mu_smooth_te,hist_l)],axis=-1)
# SSM_x = Dynamic_VAE(config_LSSM_MPP, observ=XSgn_tr_x, target=xPos_tr, states=states)
# SSM_x.parameters_opt_EM(epoches=config_LSSM_MPP['epoches'], observ=XSgn_tr_x,target=xPos_tr,
#                           maxiter=config_LSSM_MPP['maxIter'])
# [Sigma_XX,A_xx,B_xx,mu_X0,sigma_X0]= SSM_x.get_model_parms()
# [x_mu_smooth_tr, x_var_smooth_tr]=model_output_visualiztion_1D(SSM_x,config_LSSM_MPP,XSgn_tr_x,xPos_tr,'train_result_X' )
# # Test X
# [x_mu_smooth_te, x_var_smooth_te]=model_output_visualiztion_1D(SSM_x,config_LSSM_MPP,XSgn_te_x,xPos_te,'test_result_X' )
# perf_metrics_4D_x[0,:,:]=cal_performance_4D(x_mu_smooth_tr.squeeze(),x_var_smooth_tr.squeeze(),xPos_tr.squeeze(),
#                                             x_mu_smooth_te.squeeze(),x_var_smooth_te.squeeze(),xPos_te.squeeze())
#
#
# DNN_model_x=baseline_DNNs(XSgn_tr_x, xPos_tr)
# perf_metrics_DNN_x[0,:,:]=cal_baseline_DNNs_performance(DNN_model_x, XSgn_tr_x, xPos_tr.squeeze(), XSgn_te_x,
#                                                         xPos_te.squeeze(),config_LSSM_MPP)
#
# GLM_model_x=baseline_GLMs(XSgn_tr_x[:,0,:].squeeze(), xPos_tr)
# perf_metrics_GLM_x[0, :, :] = cal_baseline_GLMs_performance(GLM_model_x, XSgn_tr_x[:,0,:].squeeze(), xPos_tr, XSgn_te_x[:,0,:].squeeze(), xPos_te,config_LSSM_MPP)
#
# DDDD_result_x = vis_performance(perf_metrics_4D_x, '4D_x')
# DNN_result_x = vis_performance(perf_metrics_DNN_x, 'DNN_x')
# GLM_result_x = vis_performance(perf_metrics_GLM_x, 'GLM_x')


## 2D performance
# perf_metrics_4D_xy[0,:,:]=cal_performance_4D_2D(x_mu_smooth_tr,x_var_smooth_tr,xPos_tr,x_mu_smooth_te,x_var_smooth_te,xPos_te,
#                                              y_mu_smooth_tr,y_var_smooth_tr,yPos_tr,y_mu_smooth_te,y_var_smooth_te,yPos_te)

# tot_result = pd.concat([DDDD_result_x,DDDD_result_y,DNN_result_x,DNN_result_y,GLM_result_x,GLM_result_y])
# plt.figure()
# tot_result.boxplot()
# plt.xticks(rotation = 90)