import torch
import torch.nn.functional as F
import numpy as np
from scipy.stats import multivariate_normal

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

import seaborn as sns

from tqdm import tqdm_notebook as tqdm

# module level imports
import sys
sys.path.append("..")

from src_GMM_example.distributions import log_gaussian
from src_GMM_example.utils import logsumexp

def sample(mu, var, nb_samples=500):
    """
    Return a tensor of (nb_samples, features), sampled
    from the parameterized gaussian.
    :param mu: torch.Tensor of the means
    :param var: torch.Tensor of variances (NOTE: zero covars.)
    """
    out = []
    for i in range(nb_samples):
        out += [
            torch.normal(mu, var.sqrt())
        ]
    return torch.stack(out, dim=0)

# generate some clusters
cluster1 = sample(
    torch.Tensor([2.5, 2.5]),
    torch.Tensor([1.2, .8]),
    nb_samples=500
)

cluster2 = sample(
    torch.Tensor([7.5, 7.5]),
    torch.Tensor([.75, .5]),
    nb_samples=500
)

cluster3 = sample(
    torch.Tensor([8, 1.5]),
    torch.Tensor([.6, .8]),
    nb_samples=1000
)

def plot_2d_sample(sample):
    sample_np = sample.numpy()
    x = sample_np[:, 0]
    y = sample_np[:, 1]
    plt.scatter(x, y)

# create the dummy dataset, by combining the clusters.
X = torch.cat([cluster1, cluster2, cluster3])
plot_2d_sample(X)


def initialize(data, k, var=1):
    """
    Randomly initialize the parameters for `k` gaussians.
    :param data: design matrix (examples, features)
    :param k: number of gaussians
    :param var: initial variance
    """
    # choose k points from data to initialize means
    m = data.size(0)
    idxs = torch.from_numpy(np.random.choice(m, k, replace=False)).long()
    mu = data[idxs]

    # uniform sampling for means and variances
    var = torch.Tensor(k, d).fill_(var)

    # uniform prior
    pi = torch.empty(k).fill_(1. / k)

    return mu, var, pi


def get_likelihoods(X, mu, logvar, log=True):
    """
    Compute the likelihood of each data point under each gaussians.
    :param X: design matrix (examples, features)
    :param mu: the component means (K, features)
    :param logvar: the component log-variances (K, features)
    :param log: return value in log domain?
        Note: exponentiating can be unstable in high dimensions.
    :return likelihoods: (K, examples)
    """

    # get feature-wise log-likelihoods (K, examples, features)
    log_likelihoods = log_gaussian(
        X[None, :, :],  # (1, examples, features)
        mu[:, None, :],  # (K, 1, features)
        logvar[:, None, :]  # (K, 1, features)
    )

    # sum over the feature dimension
    log_likelihoods = log_likelihoods.sum(-1)

    if not log:
        log_likelihoods.exp_()

    return log_likelihoods

def get_posteriors(log_likelihoods, log_pi):
    """
    Calculate the the posterior probabities log p(z|x), assuming a uniform prior over
    components (for this step only).
    :param likelihoods: the relative likelihood p(x|z), of each data point under each mode (K, examples)
    :param log_pi: log prior (K)
    :return: the log posterior p(z|x) (K, examples)
    """
    posteriors = log_likelihoods # + log_pi[:, None]
    posteriors = posteriors - logsumexp(posteriors, dim=0, keepdim=True)
    return posteriors


def get_parameters(X, log_posteriors, eps=1e-6, min_var=1e-6):
    """
    :param X: design matrix (examples, features)
    :param log_posteriors: the log posterior probabilities p(z|x) (K, examples)
    :returns mu, var, pi: (K, features) , (K, features) , (K)
    """

    posteriors = log_posteriors.exp()

    # compute `N_k` the proxy "number of points" assigned to each distribution.
    K = posteriors.size(0)
    N_k = torch.sum(posteriors, dim=1)  # (K)
    N_k = N_k.view(K, 1, 1)

    # get the means by taking the weighted combination of points
    # (K, 1, examples) @ (1, examples, features) -> (K, 1, features)
    mu = posteriors[:, None] @ X[None,]
    mu = mu / (N_k + eps)

    # compute the diagonal covar. matrix, by taking a weighted combination of
    # the each point's square distance from the mean
    A = X.unsqueeze(0) - mu
    print("A {}, X {}, mu {}".format(A.size(), X.size(), mu.size()))
    var = posteriors[:, None] @ (A ** 2)  # (K, 1, features)
    var = var / (N_k + eps)
    logvar = torch.clamp(var, min=min_var).log()

    # recompute the mixing probabilities
    m = X.size(1)  # nb. of training examples
    pi = N_k / N_k.sum()

    return mu.squeeze(1), logvar.squeeze(1), pi.squeeze()


def get_density(mu, logvar, pi, N=50, X_range=(0, 5), Y_range=(0, 5)):
    """ Get the mesh to compute the density on. """
    X = np.linspace(*X_range, N)
    Y = np.linspace(*Y_range, N)
    X, Y = np.meshgrid(X, Y)

    # get the design matrix
    points = np.concatenate([X.reshape(-1, 1), Y.reshape(-1, 1)], axis=1)
    points = torch.from_numpy(points).float()

    # compute the densities under each mixture
    P = get_likelihoods(points, mu, logvar, log=False)

    # sum the densities to get mixture density
    Z = torch.sum(P, dim=0).data.numpy().reshape([N, N])

    return X, Y, Z
def plot_density(X, Y, Z, i=0):
    fig = plt.figure(figsize=(10, 10))
    ax = fig.gca(projection='3d')
    ax.plot_surface(X, Y, Z, rstride=3, cstride=3, linewidth=1, antialiased=True,
                    cmap=cm.inferno)
    cset = ax.contourf(X, Y, Z, zdir='z', offset=-0.15, cmap=cm.inferno)

    # adjust the limits, ticks and view angle
    ax.set_zlim(-0.15,0.2)
    ax.set_zticks(np.linspace(0,0.2,5))
    ax.view_init(27, -21)
#     plt.savefig('fig_{}.png'.format(i), dpi=400, bbox_inches='tight')
    plt.show()


# training loop
k = 3
d = 2
nb_iters = 50

data = X
mu, var, pi = initialize(data, k, var=1)
logvar = var.log()

prev_cost = float('inf')
thresh = 1e-5
for i in tqdm(range(nb_iters)):
    # get the likelihoods p(x|z) under the parameters
    log_likelihoods = get_likelihoods(data, mu, logvar)

    # plot!
    plot_density(*get_density(mu, logvar, pi, N=100, X_range=(-2, 12), Y_range=(-2, 12)), i=i)

    # compute the "responsibilities" p(z|x)
    log_posteriors = get_posteriors(log_likelihoods, pi.log())

    # compute the cost and check for convergence
    cost = log_likelihoods.mean()
    diff = prev_cost - cost
    if torch.abs(diff).item() < thresh:
        break
    prev_cost = cost

    # re-compute parameters
    mu, logvar, pi = get_parameters(data, log_posteriors)