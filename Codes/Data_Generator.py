import numpy as np
from scipy.special import  expit



'''Synthetic Dataset '''


'''
generate a 1-D random walk state and Gaussian and Bernoulli observations based on the state values in each timepoint 
'''
class Data_Generator:
    def __init__(self, config):
        self.number_sample =int(config['simulation_duration'] / config['dt']) # number of data point
        self.dt = config['dt'] # time resolution
        self.alphaX = config['alphaX']
        self.sigmaX = config['sigmaX']
        self.initial_X = config['initial_X']
        self.lim_X=config['lim_X']
        self.X= np.zeros([self.number_sample, 1])
        self.X[0] = self.initial_X


        self.number_LFPs = config['number_LFPs']
        self.LFPs_sigm = config['LFPs_sigma']
        self.LFPs_alpha = config['LFPs_alpha']
        self.LFPs_init = config['LFPs_init']
        self.LFPs = np.zeros([self.number_sample, self.number_LFPs])


    ''' models p(x_k|x_(k-1) ~ N (\alpha x_(k-1), \sigma) where x_k is a 2D state'''
    def generate_state_fun(self):
        for i in range(1, self.number_sample-1):
            temp=self.alphaX * self.X[i] + np.random.normal(0, self.sigmaX) # 1D random walk
            if (temp > self.lim_X[0]) and (temp < self.lim_X[1]): # keep the state values bounded
                self.X[i+1] = temp
            else:
                self.X[i+1] = self.X[i]
        return self.X



    def generate_LFPs_obs_fun(self):
        ''' generate multi-variate Gaussian observations from the x-state '''
        for j in range(self.number_LFPs):
            # Relu nonlinearity
            if j %2 == 0:
                for i in range(self.number_sample):
                    self.LFPs[i, j] = np.random.normal(np.tanh( np.squeeze((self.X[i]) * self.LFPs_alpha[j])), self.LFPs_sigm[j])
            elif j %2 == 0:
                for i in range(self.number_sample):
                    self.LFPs[i, j] = np.random.normal(max(0.0, np.squeeze((self.X[i]) * self.LFPs_alpha[j])), self.LFPs_sigm[j])

            elif j % 3== 0:
                for i in range(self.number_sample):
                    self.LFPs[i, j]= np.random.normal(expit(np.squeeze((self.X[i]) * self.LFPs_alpha[j])),self.LFPs_sigm[j])
            # else:
            #     for i in range(self.number_sample):
            #         self.LFPs[i, j]= np.random.normal(np.squeeze((self.X[i]) * self.LFPs_alpha[j]),self.LFPs_sigm[j])
        return self.LFPs


