from D4_greedy import D4_greedy
from Data_Generator_V2 import Data_Generator_V2,cov_gen
import numpy as np
from scipy.stats import pearsonr
from utils import *
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold
number_LFP_chs = 20
num_folds=5
kf = KFold(n_splits=num_folds, random_state=None, shuffle=False)

perf_metrics_4D=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_DNN=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
perf_metrics_SSM=np.zeros((num_folds,3,2)) # folds, metric(0-> MAE, 1->MSE, 2-> 95% hpd),  train/test(0/1)
'''  '''
config_synthetic_Data = {  ## set parameters fpr  x state
        'alphaX': 1,  # state transition Coff
        'sigmaX': .1,  # noise variance
        'initial_X': 0,  # state initialization
        'lim_X': [-1, 1],  # bounds for possible values for the state x
        'h_K':5,
        ## parameters for the LFP signals generation
        'number_LFPs': number_LFP_chs,
        'LFPs_sigma': cov_gen(.3,.5,number_LFP_chs,5),
        'LFPs_init': np.zeros([number_LFP_chs, 1]).squeeze(),
        'LFPs_alpha': np.ones( (number_LFP_chs,)),
        ## simulation time
        'dt': 0.001,
        'simulation_duration': 1,
}
config_LSSM_MPP = {
        'X_bounds': [-2, 2],  # define lower and upper bound of state estimation
        'X_length': 400,  # the whole state bound quantized tp X_length values for numerical calculations
        'number_MCS':100,  # number of trajectory samples for expectation calculations in EM step
        'opt_method': 'Simple-sampling',  # optimization method for expectation calculation
        'epoches': 5, # maximum number of iterations for fminsearch the parameters optimization
        'maxIter':400, # maximum number of iterations for the loop of parameters optimization

        'training_mode': 'un_supervised' # 'supervised' or 'un_supervised'

    }
# define the states
states=np.linspace(config_LSSM_MPP['X_bounds'][0],config_LSSM_MPP['X_bounds'][1], config_LSSM_MPP['X_length'])
states = states.reshape([-1, 1])

Synth_gen = Data_Generator_V2(config_synthetic_Data)
X = Synth_gen.generate_state_fun()
X=((X-np.min(X))/(np.max(X)-np.min(X))-.5)*(config_LSSM_MPP['X_bounds'][1]-config_LSSM_MPP['X_bounds'][0])
Neural_Features_d = Synth_gen.generate_LFPs_obs_fun()

plt.figure()
plt.plot(Neural_Features_d)

plt.figure()
plt.plot(X)