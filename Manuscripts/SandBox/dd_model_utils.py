import numpy as np
import scipy.io as sio
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from scipy.linalg import hankel
import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions
import statsmodels.api as sm
from scipy.stats import norm
import pandas as pd
import scipy.optimize
def calInformetiveChan(data,minNumSpiks):

    return np.where(np.sum(data,axis=0)>minNumSpiks)

def calSmoothNeuralActivity(data,gausWindowLength,gausWindowSigma):
    x=np.linspace(-1*gausWindowSigma,1*gausWindowSigma,gausWindowLength)
    gausWindow=1/(2*np.pi*gausWindowSigma)*np.exp(-0.5*(x**2/gausWindowSigma**2))
    gausWindow=gausWindow/np.max(gausWindow)
    #plt.plot(x,gausWindow)
    #plt.show()
    dataSmooth=np.zeros(data.shape)
    for i in range(data.shape[1]):
        dataSmooth[:,i]=np.convolve(data[:,i],gausWindow,'same')
        #dataSmooth[np.where(dataSmooth[:,i] <0), i]=0
    #plt.subplot(2,1,1)
    #plt.plot(data[:5000,1])
    #plt.subplot(2, 1, 2)
    #plt.plot(dataSmooth[:5000, 1])
    #plt.show()
    return dataSmooth

def calDesignMatrix(X,h):
    PadX = np.zeros([h , X.shape[1]])
    PadX =np.concatenate([PadX,X],axis=0)
    XDsgn=np.zeros([X.shape[0], X.shape[1]*h])
    for i in range(X.shape[1]):
         XDsgn[:, i* h : (i+1) * h]= hankel(PadX[:  - h , i],PadX[ - h :, i])
    return XDsgn

from tensorflow.keras.layers import Layer
from tensorflow.keras import activations, initializers
from tensorflow.keras import backend as K
class DenseVariational(Layer):
    def __init__(self,
                 units,
                 kl_weight,
                 activation=None,
                 prior_sigma_1=1.5,
                 prior_sigma_2=0.1,
                 prior_pi=0.5, **kwargs):
        self.units = units
        self.kl_weight = kl_weight
        self.activation = activations.get(activation)
        self.prior_sigma_1 = prior_sigma_1
        self.prior_sigma_2 = prior_sigma_2
        self.prior_pi_1 = prior_pi
        self.prior_pi_2 = 1.0 - prior_pi
        self.init_sigma = np.sqrt(self.prior_pi_1 * self.prior_sigma_1 ** 2 +
                                  self.prior_pi_2 * self.prior_sigma_2 ** 2)

        super().__init__(**kwargs)

    def compute_output_shape(self, input_shape):
        return input_shape[0], self.units

    def build(self, input_shape):
        self.kernel_mu = self.add_weight(name='kernel_mu',
                                         shape=(input_shape[1], self.units),
                                         initializer=initializers.random_normal(stddev=self.init_sigma),
                                         trainable=True)
        self.bias_mu = self.add_weight(name='bias_mu',
                                       shape=(self.units,),
                                       initializer=initializers.random_normal(stddev=self.init_sigma),
                                       trainable=True)
        self.kernel_rho = self.add_weight(name='kernel_rho',
                                          shape=(input_shape[1], self.units),
                                          initializer=initializers.constant(0.0),
                                          trainable=True)
        self.bias_rho = self.add_weight(name='bias_rho',
                                        shape=(self.units,),
                                        initializer=initializers.constant(0.0),
                                        trainable=True)
        super().build(input_shape)

    def call(self, inputs, **kwargs):
        kernel_sigma = tf.math.softplus(self.kernel_rho)
        kernel = self.kernel_mu + kernel_sigma * tf.random.normal(self.kernel_mu.shape)

        bias_sigma = tf.math.softplus(self.bias_rho)
        bias = self.bias_mu + bias_sigma * tf.random.normal(self.bias_mu.shape)

        self.add_loss(self.kl_loss(kernel, self.kernel_mu, kernel_sigma) +
                      self.kl_loss(bias, self.bias_mu, bias_sigma))

        return self.activation(K.dot(inputs, kernel) + bias)

    def kl_loss(self, w, mu, sigma):
        variational_dist = tfp.distributions.Normal(mu, sigma)
        return self.kl_weight * K.sum(variational_dist.log_prob(w) - self.log_prior_prob(w))

    def log_prior_prob(self, w):
        comp_1_dist = tfp.distributions.Normal(0.0, self.prior_sigma_1)
        comp_2_dist = tfp.distributions.Normal(0.0, self.prior_sigma_2)
        return K.log(self.prior_pi_1 * comp_1_dist.prob(w) +
                     self.prior_pi_2 * comp_2_dist.prob(w))

def neg_log_likelihood(y_obs, y_pred, sigma=.1):
    dist = tfp.distributions.Normal(loc=y_pred, scale=sigma)
    return 1*K.sum(-dist.log_prob(y_obs)) +0*tf.math.reduce_sum( tf.keras.losses.MSE(y_obs,y_pred))
negloglik = lambda y, p_y: -p_y.log_prob(y) +1*tf.math.reduce_sum( tf.keras.losses.MSE(y,p_y))

class RBFKernelFn(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super(RBFKernelFn, self).__init__(**kwargs)
        dtype = kwargs.get('dtype', None)

        self._amplitude = self.add_variable(
            initializer=tf.constant_initializer(0),
            dtype=dtype,
            name='amplitude')

        self._length_scale = self.add_variable(
            initializer=tf.constant_initializer(0),
            dtype=dtype,
            name='length_scale')

    def call(self, x):
        # Never called -- this is just a layer so it can hold variables
        # in a way Keras understands.
        return x

    @property
    def kernel(self):
        return tfp.math.psd_kernels.ExponentiatedQuadratic(
            amplitude=tf.nn.softplus(0.1 * self._amplitude),
            length_scale=tf.nn.softplus(5. * self._length_scale)
        )
# Specify the surrogate posterior over `keras.layers.Dense` `kernel` and `bias`.
def posterior_mean_field(kernel_size, bias_size=0, dtype=None):
  n = kernel_size + bias_size
  c = np.log(np.expm1(1.))
  return tf.keras.Sequential([
      tfp.layers.VariableLayer(2 * n, dtype=dtype),
      tfp.layers.DistributionLambda(lambda t: tfd.Independent(
          tfd.Normal(loc=t[..., :n],
                     scale=1e-5 + tf.nn.softplus(c + t[..., n:])),
          reinterpreted_batch_ndims=1)),
  ])
# Specify the prior over `keras.layers.Dense` `kernel` and `bias`.
def prior_trainable(kernel_size, bias_size=0, dtype=None):
  n = kernel_size + bias_size
  return tf.keras.Sequential([
      tfp.layers.VariableLayer(n, dtype=dtype),
      tfp.layers.DistributionLambda(lambda t: tfd.Independent(
          tfd.Normal(loc=t, scale=1),
          reinterpreted_batch_ndims=1)),
  ])

def parInit(Data,stateBound):
    Par={}
    Par['mu0'] = 0
    Par['sigma0'] = .5

    Par['Ak']= 1
    Par['Bk']= 0
    Par['sigmaX'] = .5
    Par['muX'] = 0

    Par['a0']= -2
    Par['a1']= 1
    Par['a2'] = 1.294393335263568
    Par['a3'] = -1.303739651630918

    M=Data['SH'].shape[0]
    N = Data['SH'].shape[1]



    # GLM_model = sm.GLM(Data['x'], Data['SH'], family=sm.families.Gaussian(), )
    # GLM_results = GLM_model.fit()
    # print(GLM_results.summary())
    #
    # linear_response=np.dot(Data['SH'],GLM_results.params)
    # plt.subplot(2,1,1)
    # plt.plot(Data['x'],'k')
    # plt.subplot(2,1,2)
    # plt.plot(linear_response,'b')
    # plt.show()

    Par['XSHw'] =stateBound * np.random.random([N + 1, 1]) / np.max(np.max(Data['Ycells'])) / len(Data['bestIdx'])/2
    #Par['XSHw']=GLM_results.params
    #Par['XSHw']=np.concatenate([np.array([0]),Par['XSHw']],axis=0)
    #Par['XSHw']=Par['XSHw'].reshape([-1,1])
    Par['XSHs'] = .5
    Par['XSHm'] = 0
    return Par

def R_Pzx(Data, Par, xVals, ii):
    '''compute p(zk|xk)'''
    ipk =Data['pwords'][ np.where((Data['t'][ii - 1] < Data['pwords']) & (Data['pwords'] <= Data['t'][ii]))]
    iqk =Data['cwords'][ np.where((Data['t'][ii - 1] < Data['cwords']) & (Data['cwords'] <= Data['t'][ii]))]
    ipkc = Data['pwords'][np.where((Data['t'][ii - 1] - Data['ZHis'] < Data['pwords']) & (Data['pwords'] <= Data['t'][ii]))]
    iqkc = Data['cwords'][np.where((Data['t'][ii - 1] - Data['ZHis'] < Data['cwords'] ) & (Data['cwords'] <= Data['t'][ii]))]

    if len(ipk) !=0  and len(iqk)!=0:

        zk = 1
        pk = len(ipkc)
        qk = len(iqkc)
        Landa = np.exp(Par['a0'] + Par['a1'] * xVals + Par['a2'] * pk +Par['a3'] * qk)
        Pzx = np.exp(-Landa) * Landa
        if Pzx.sum() !=0:
            Pzx = Pzx / Pzx.sum()

    elif len(ipk)==0 and len(iqk)!=0:

        zk = 0
        pk = 0
        qk = len(iqkc)
        Landa = np.exp(Par['a0'] +Par['a1'] * xVals + Par['a3'] * qk)
        Pzx = np.exp(-Landa)

        if Pzx.sum() !=0:
            Pzx = Pzx / Pzx.sum()

    elif len(ipk)!=0 and len(iqk)==0:
        zk = 1
        pk = len(ipkc)
        qk = 0
        Landa = np.exp(Par['a0'] + Par['a1'] * xVals + Par['a2'] * pk)
        Pzx = np.exp(-Landa)* Landa
        if Pzx.sum() != 0:
            Pzx = Pzx / Pzx.sum()

    else:
        pk = 0
        qk = 0
        zk = 0
        Landa = np.exp(Par['a0'] + Par['a1'] * xVals)
        Pzx = np.exp(-Landa)
        if Pzx.sum() != 0:
            Pzx = Pzx / Pzx.sum()

    return Pzx,pk,qk,zk

def ExpectedQ_PXS_Ns(par,xvals, PXkYT, SH, dvd, sigmav, DataTrain):
    #xvals, PXkYT, SH, dvd, sigmav, DataTrain=arg
    '''Pxs parameters'''
    w=par

    xmu1 = DataTrain['SH'][:-1, :].dot(w[1:]) + w[0]
    xmu1 = (xmu1 - xmu1.min()) / (xmu1.max() - xmu1.min())
    xmu1=xmu1.reshape([-1, 1])
    IndxXS = range(dvd, PXkYT.shape[0], dvd)



    Exs=np.nansum(-0.5 * np.log(sigmav**2) -0.5/(sigmav**2) * (((PXkYT[IndxXS, :]).dot(xvals ** 2)).reshape([-1, 1])\
                                                         +xmu1.reshape([-1,1])**2 -2*xmu1 * ((PXkYT[IndxXS, :]).dot(xvals )).reshape([-1, 1])))
    UperLimitQ=-1*np.squeeze(Exs)
    return UperLimitQ

def  ExpectedQ_PXZ(par,xhat,xvals,PXkYT,pw,qw,zk,a0,a1):
    a2 = par[0]
    a3 = par[1]
    temp1=np.exp(a0, a1*xhat[1:]+ a2*pw[1:] + a3*qw[1:])
    temp2=PXkYT[1:,:].dot(xvals)
    temp3 = PXkYT[1:, :].dot(xvals**2)
    Ezx=np.nansum(-1* (temp1+ a1 * temp1 * temp2 - xhat[1:,:] +
               .5 * a1**2 *(temp3 - 2* xhat[1:,:]* temp2 + xhat[1:,:]**2) * temp1)+
               zk[1:] * (a0 + a1 * temp2+ a2*pw[1:] + a3*qw[1:] ))
    UperLimitQ =-1*Ezx
    return UperLimitQ

def  ExpectedQ_PX0(par,xvals,PXkYT):
    #Px0 parameters
    m0 = par[0]
    sigma0 = par[1]

    Ex0=np.nansum(-0.5 * np.log(sigma0**2) -0.5/sigma0**2 * (PXkYT[1,:].dot(xvals**2) + m0**2 -2*m0*PXkYT[1,:].dot(xvals )))

    UperLimitQ=-1*(Ex0)
    return UperLimitQ
def  ExpectedQ_PXX(par,xvals,PXkYT,epj,dvd):

    #Pxx parameters
    sigmae = par
    #pz parameters

    IndxXSd = range(dvd, PXkYT.shape[0], dvd)
    IndxXS = range(0, PXkYT.shape[0]-dvd, dvd)
    Exxj=np.nansum(-0.5 * np.log(sigmae**2) -0.5/sigmae**2*(epj[:-1]))
    Exx=np.nansum(-0.5 * np.log(dvd*sigmae**2) -0.5/(dvd*sigmae**2)*(PXkYT[IndxXSd,:].dot(xvals**2)  +
                                                               PXkYT[IndxXS,:].dot(xvals**2) - 2 *PXkYT[IndxXSd,:].dot(xvals) *PXkYT[IndxXS,:].dot(xvals) ))

    UperLimitQ=-1*(Exxj-Exx)
    return UperLimitQ