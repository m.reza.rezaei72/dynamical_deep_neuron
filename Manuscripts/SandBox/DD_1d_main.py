import numpy as np
import scipy.io as sio
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from scipy.linalg import hankel
import tensorflow as tf
import tqdm
import seaborn as sns
import tensorflow_probability as tfp
tfd = tfp.distributions
import warnings
warnings.filterwarnings('ignore')

from keras.layers import Input
from keras.models import Model
from tensorflow.keras import callbacks, optimizers
import statsmodels.api as sm
from scipy.stats import norm
import pandas as pd
from dd_model_utils import *

''' load the dataset
data spec: MSTrain:27305x67 (time resolution 33ms)
col[0] : time index
col[1-62] : spiking activities
col[63] : x-position
col[64] : y-position
col[65] : x-velocity
col[66] : y-velocity
'''

Data=sio.loadmat('./datasets/Data33.mat')['MSTrain']

'''preprocess the data; finding informative channels of spikes and
 smoothing them with a gaussian window'''


NeuralData=Data[:,1:63]
bestIndxs=calInformetiveChan(NeuralData,100)




YCells=calSmoothNeuralActivity(np.squeeze(NeuralData[:,bestIndxs]),100,20)
Data[:,63]=(Data[:,63]-np.min(Data[:,63]))/(np.max(Data[:,63])-np.min(Data[:,63]))
Data[:,64]=(Data[:,64]-np.min(Data[:,64]))/(np.max(Data[:,64])-np.min(Data[:,64]))
(YCells_tr,YCells_te,xPos_tr,xPos_te,yPos_tr,yPos_te,t_tr,t_te)=train_test_split(YCells, Data[:,63],Data[:,64],Data[:,0], test_size=.2,random_state=0,shuffle=False) # split test and train daata

#plt.subplot(2,1,1)
#plt.plot(xPos_tr)
#plt.subplot(2, 1, 2)
#plt.plot(yPos_tr)
#plt.show()

model_params = {'Ak': 1.0,
                'd_step': 0.025,
                'Bk': 0.0,
                'sxx':1,
                'history_term':2,
                'Max_step':100,
                'std_x':np.std(xPos_tr),
                'mean_x':np.mean(xPos_tr)}

dd_model={'model_params':model_params,
          }


XSgn_tr=calDesignMatrix(YCells_tr,model_params['history_term'])
XSgn_te=calDesignMatrix(YCells_te,model_params['history_term'])
#plt.matshow(XSgn_tr[:1000,:])
#plt.show()

#model_coefficients_start = tf.zeros(XSgn_tr.shape[-1], np.float64)

#model_coefficients, linear_response, is_converged, num_iter = tfp.glm.fit(
#    model_matrix=tf.convert_to_tensor(XSgn_tr),
#    response=tf.convert_to_tensor(yPos_tr),
#    model=tfp.glm.Normal(),
#    model_coefficients_start=model_coefficients_start)



#print(('is_converged: {}\n'
#       '    num_iter: {}\n'

#       ).format(
#    is_converged,
#    num_iter))

#GLM_model = sm.GLM(xPos_tr,XSgn_tr , family=sm.families.Gaussian())
#GLM_results = GLM_model.fit()
#print(GLM_results.summary())

#linear_response=np.dot(XSgn_te,GLM_results.params)
#plt.subplot(2,1,1)
#plt.plot(xPos_te,'k')
#plt.subplot(2,1,2)
#plt.plot(linear_response,'b')
#plt.show()
############################### BNN ####################
# @title Custom PSD Kernel
class RBFKernelFn(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super(RBFKernelFn, self).__init__(**kwargs)
        dtype = kwargs.get('dtype', None)

        self._amplitude = self.add_variable(
            initializer=tf.constant_initializer(0),
            dtype=dtype,
            name='amplitude')

        self._length_scale = self.add_variable(
            initializer=tf.constant_initializer(0),
            dtype=dtype,
            name='length_scale')

    def call(self, x):
        # Never called -- this is just a layer so it can hold variables
        # in a way Keras understands.
        return x

    @property
    def kernel(self):
        return tfp.math.psd_kernels.ExponentiatedQuadratic(
            amplitude=tf.nn.softplus(0.1 * self._amplitude),
            length_scale=tf.nn.softplus(5. * self._length_scale)
        )

# Specify the surrogate posterior over `keras.layers.Dense` `kernel` and `bias`.
def posterior_mean_field(kernel_size, bias_size=0, dtype=None):
  n = kernel_size + bias_size
  c = np.log(np.expm1(1.))
  return tf.keras.Sequential([
      tfp.layers.VariableLayer(2 * n, dtype=dtype),
      tfp.layers.DistributionLambda(lambda t: tfd.Independent(
          tfd.Normal(loc=t[..., :n],
                     scale=1e-5 + tf.nn.softplus(c + t[..., n:])),
          reinterpreted_batch_ndims=1)),
  ])
# Specify the prior over `keras.layers.Dense` `kernel` and `bias`.
def prior_trainable(kernel_size, bias_size=0, dtype=None):
  n = kernel_size + bias_size
  return tf.keras.Sequential([
      tfp.layers.VariableLayer(n, dtype=dtype),
      tfp.layers.DistributionLambda(lambda t: tfd.Independent(
          tfd.Normal(loc=t, scale=1),
          reinterpreted_batch_ndims=1)),
  ])
tf.keras.backend.set_floatx('float64')
# Build model.
num_inducing_points = 40
model = tf.keras.Sequential([
    tf.keras.layers.InputLayer(input_shape=[XSgn_tr.shape[1]]),

    tfp.layers.DenseVariational(20, posterior_mean_field, prior_trainable, kl_weight=1/XSgn_tr.shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :10],
                           scale=1e-3 + tf.math.softplus(0.01 * t[...,10:]))),

    tfp.layers.DenseVariational(20, posterior_mean_field, prior_trainable, kl_weight=1/XSgn_tr.shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :10],
                           scale=1e-3 + tf.math.softplus(0.01 * t[...,10:]))),

    tfp.layers.DenseVariational(2, posterior_mean_field, prior_trainable, kl_weight=1/XSgn_tr.shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :1],
                           scale=1e-3 + tf.math.softplus(0.01 * t[...,1:]))),
    #tf.keras.layers.Dense(1, kernel_initializer='ones', use_bias=False),
    #tfp.layers.VariationalGaussianProcess(
    #    num_inducing_points=num_inducing_points,
    #    kernel_provider=RBFKernelFn(),
    #    event_shape=[1],
    #    inducing_index_points_initializer=tf.constant_initializer(
    #        np.linspace(0,1, num=num_inducing_points,
    #                    dtype=XSgn_tr.dtype)[..., np.newaxis]),
    #    unconstrained_observation_noise_variance_initializer=(
    #        tf.constant_initializer(np.array(0.54).astype(XSgn_tr.dtype))),
    #),
])

# Do inference.
batch_size = 16
#loss = lambda y, rv_y: rv_y.variational_loss(
#    y, kl_weight=np.array(batch_size, XSgn_tr.dtype) / XSgn_tr.shape[0])
model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.01), loss=negloglik)
model.fit(XSgn_tr, xPos_tr, batch_size=batch_size, epochs=100, verbose=1)

Numb_SampleingTimes=20
# Make predictions.
x_preds=np.zeros([XSgn_te.shape[0],Numb_SampleingTimes])
yhat=np.zeros([XSgn_te.shape[0],Numb_SampleingTimes])
for i in range(Numb_SampleingTimes):

    yhat[:,i]=np.squeeze(model.predict(XSgn_te))


x_mean = np.mean(yhat,axis=-1)
x_sigma = np.std(yhat,axis=-1)
##################
xVals=np.linspace(model_params['mean_x']-4*model_params['std_x'],
                  model_params['mean_x']+4*model_params['std_x'],
                  model_params['Max_step'])
dd_model['px']=norm.pdf(xVals, model_params['mean_x'],model_params['std_x'])

nTimeValues= xPos_te.shape[0]
nStateValues = xVals.shape[0]
dd_model['P']=np.zeros((nTimeValues,nStateValues))
dd_model['xm']=np.zeros((nTimeValues,1))
dd_model['xs']=np.zeros((nTimeValues,1))
dd_model['Pxx']=np.zeros((nStateValues,nStateValues))

for i in range(nStateValues):
    m=model_params['Ak']*xVals[i]+model_params['Bk']
    dd_model['Pxx'][i,:]= norm.pdf(xVals, m,model_params['std_x'])

for i in range(nStateValues):

    dd_model['Pxx'][:,i]= dd_model['Pxx'][:,i]/np.sum(dd_model['Pxx'][:,i])

Pxz_prv = dd_model['Pxx'][1,:]
xmu=xPos_te[0]
hpd=0

for ii in range(nTimeValues):
    print ('%d/%d \n'%(ii,nTimeValues))
    dd_model['px']=np.dot(dd_model['Pxx'],Pxz_prv)
    if ii ==0:
        dd_model['P'][ii,:]=dd_model['px']
    else:
        #compute p(xk|zk,hk) using variance of x|z (as computed from GLM)
        Pxz = norm.pdf(xVals, x_mean[ii], x_sigma[ii])
        Pxz_prv = Pxz
        #compute p(xk|z1,...,zk-1) = integral(p(xk|xk-1) * Pprev)
        Pint =np.dot(dd_model['Pxx'],dd_model['P'][ii-1,:].transpose())
        # compute P = p(xk|zk) * p(xk|z1,...,zk-1) / p(xk)
        p=(Pxz *Pint)/ dd_model['px']
        dd_model['P'][ii, :]=p/np.sum(p)
        dd_model['xm'][ii]=np.dot(xVals,dd_model['P'][ii, :].transpose())
        dd_model['xs'][ii] = np.dot(xVals**2, dd_model['P'][ii, :].transpose())-dd_model['xm'][ii]**2


plt.plot(t_te,xPos_te,'k')
#plt.subplot(2,1,2)
plt.plot(t_te,x_mean,'b')
plt.plot(t_te,dd_model['xm'],':r',alpha=.8)
plt.fill_between(t_te, np.squeeze(dd_model['xm']-2*dd_model['xs']), np.squeeze(dd_model['xm']+2*dd_model['xs']), color='g', alpha=.5)

plt.show()