import numpy as np
import scipy.io as sio
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from scipy.linalg import hankel
from scipy.stats import multivariate_normal
import tensorflow as tf
import tqdm
import seaborn as sns
import tensorflow_probability as tfp
tfd = tfp.distributions
import warnings
warnings.filterwarnings('ignore')
np.random.seed(0)
from keras.layers import Input
from keras.models import Model
from tensorflow.keras import callbacks, optimizers
import statsmodels.api as sm
from scipy.stats import norm
import pandas as pd
from dd_model_utils import *
from sklearn.metrics import mean_squared_error
''' load the dataset
data spec: MSTrain:27305x67 (time resolution 33ms)
col[0] : time index
col[1-62] : spiking activities
col[63] : x-position
col[64] : y-position
col[65] : x-velocity
col[66] : y-velocity
'''

Data=sio.loadmat('./datasets/Data33.mat')['MSTrain']

'''preprocess the data; finding informative channels of spikes and
 smoothing them with a gaussian window'''


NeuralData=Data[:,1:63]
bestIndxs=calInformetiveChan(NeuralData,120)




YCells=calSmoothNeuralActivity(np.squeeze(NeuralData[:,bestIndxs]),60,15)
Data[:,63]=(Data[:,63]-np.min(Data[:,63]))/(np.max(Data[:,63])-np.min(Data[:,63]))
Data[:,64]=(Data[:,64]-np.min(Data[:,64]))/(np.max(Data[:,64])-np.min(Data[:,64]))
xPos=Data[:,63]
yPos=Data[:,64]
(YCells_tr,YCells_te,xPos_tr,xPos_te,yPos_tr,yPos_te,t_tr,t_te)=train_test_split(YCells, Data[:,63],Data[:,64],Data[:,0], test_size=.2,random_state=0,shuffle=False) # split test and train daata

#plt.subplot(2,1,1)
#plt.plot(xPos_tr)
#plt.subplot(2, 1, 2)
#plt.plot(yPos_tr)
#plt.show()

model_params = {'Ak': np.eye(2),
                'Bk': np.array([0,0]).transpose(),
                'Qk': np.diag([np.std(xPos_tr),np.std(yPos_tr)]),
                'sxy':np.eye(2),
                'history_term':np.array([2,2]).transpose(),
                'Max_step':100,
                'std_xy':np.array([np.std(xPos_tr),np.std(yPos_tr)]).transpose(),
                'mean_xy':np.array([np.mean(xPos_tr),np.mean(yPos_tr)]).transpose()}

dd_model={'model_params':model_params,
          }

################### BNN Y ###################
XSgn_y_tr=calDesignMatrix(YCells_tr,model_params['history_term'][1])
XSgn_y_te=calDesignMatrix(YCells_te,model_params['history_term'][1])
tf.keras.backend.set_floatx('float64')
# Build model.
modely = tf.keras.Sequential([
    tf.keras.layers.InputLayer(input_shape=[XSgn_y_tr.shape[1]]),

    tfp.layers.DenseVariational(20, posterior_mean_field, prior_trainable, kl_weight=1/XSgn_y_tr.shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :10],
                           scale=1e-4 + tf.math.softplus(0.01 * t[...,10:]))),



    tfp.layers.DenseVariational(2, posterior_mean_field, prior_trainable, kl_weight=1/XSgn_y_tr.shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :1],
                           scale=1e-4 + tf.math.softplus(0.001 * t[...,1:]))),

])

batch_size = 16
Num_epochs=50
modely.compile(optimizer=tf.optimizers.Adam(learning_rate=0.01), loss=negloglik)
modely.fit(XSgn_y_tr, yPos_tr, batch_size=batch_size, epochs=Num_epochs, verbose=1)

Numb_SampleingTimes=20
# Make predictions.

yhat_te=np.zeros([XSgn_y_te.shape[0],Numb_SampleingTimes])
for i in range(Numb_SampleingTimes):
    yhat_te[:,i]=np.squeeze(modely.predict(XSgn_y_te))
y_mean_te = np.mean(yhat_te,axis=-1)

yhat_tr=np.zeros([XSgn_y_tr.shape[0],Numb_SampleingTimes])
for i in range(Numb_SampleingTimes):
    yhat_tr[:,i]=np.squeeze(modely.predict(XSgn_y_tr))
y_mean_tr = np.mean(yhat_tr,axis=-1)
############## BNN X ###############
XSgn_x_tr=calDesignMatrix(np.concatenate([YCells_tr,y_mean_tr.reshape([-1,1])],axis=-1),
                          model_params['history_term'][0])
XSgn_x_te=calDesignMatrix(np.concatenate([YCells_te,y_mean_te.reshape([-1,1])],axis=-1),
                          model_params['history_term'][0])
modelx = tf.keras.Sequential([
    tf.keras.layers.InputLayer(input_shape=[XSgn_x_tr.shape[1]]),

    tfp.layers.DenseVariational(20, posterior_mean_field, prior_trainable, kl_weight=1/XSgn_x_tr.shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :10],
                           scale=1e-4 + tf.math.softplus(0.01 * t[...,10:]))),



    tfp.layers.DenseVariational(2, posterior_mean_field, prior_trainable, kl_weight=1/XSgn_x_tr.shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :1],
                           scale=1e-4 + tf.math.softplus(0.001 * t[...,1:]))),

])

modelx.compile(optimizer=tf.optimizers.Adam(learning_rate=0.01), loss=negloglik)
modelx.fit(XSgn_x_tr, xPos_tr, batch_size=batch_size, epochs=Num_epochs, verbose=1)


yhat=np.zeros([XSgn_y_te.shape[0],2,Numb_SampleingTimes])
for i in range(Numb_SampleingTimes):

    yhat[:,0,i]=np.squeeze(modelx.predict(XSgn_x_te))
    yhat[:, 1, i] = np.squeeze(modely.predict(XSgn_y_te))
x_mean = np.mean(yhat,axis=-1)
x_sigma = np.std(yhat,axis=-1)


plt.subplot(2,1,1)
plt.plot(t_te,xPos_te,'k',linewidth=2)
plt.plot(t_te,x_mean[:,0],'b',linewidth=1)
plt.fill_between(t_te, np.squeeze(x_mean[:,0]-2*x_sigma[:,0]),
                 np.squeeze(x_mean[:,0]+2*x_sigma[:,0]), color='g', alpha=.3)
plt.subplot(2,1,2)
plt.plot(t_te,yPos_te,'k',linewidth=2)
plt.plot(t_te,x_mean[:,1],'b',linewidth=1)
plt.fill_between(t_te, np.squeeze(x_mean[:,1]-2*x_sigma[:,1]),
                 np.squeeze(x_mean[:,1]+2*x_sigma[:,1]), color='g', alpha=.3)

plt.show()
##################

xVals=np.linspace(model_params['mean_xy'][0]-4*model_params['std_xy'][0],
                  model_params['mean_xy'][0]+4*model_params['std_xy'][0],
                  model_params['Max_step'])

yVals=np.linspace(model_params['mean_xy'][1]-4*model_params['std_xy'][1],
                  model_params['mean_xy'][1]+4*model_params['std_xy'][1],
                  model_params['Max_step'])

[xValM,yValM]=np.meshgrid(xVals,yVals)
posTemp = np.dstack((xValM,yValM))
MVN=multivariate_normal( model_params['mean_xy'],model_params['Qk'])
dd_model['px']=MVN.pdf(posTemp)
#fig2 = plt.figure()
#ax2 = fig2.add_subplot(111)
#ax2.contourf(xValM, yValM, dd_model['px'])
#plt.show()
dd_model['px']=np.stack(dd_model['px'])
nTimeValues= xPos_te.shape[0]
nStateValues = xVals.shape[0]*yVals.shape[0]

############################ Plenlaty ###########
Errm=np.ones([nStateValues,])
truePosition=np.concatenate([xPos.reshape([-1,1]),yPos.reshape([-1,1])],
                            axis=-1)
for indM in range(nStateValues):
    extendPos=(np.squeeze(posTemp.reshape([-1,2])[indM,:]).transpose().reshape([2,1])*np.ones([2,yPos.shape[0]])).transpose()

    Errm[indM]=(np.sqrt(((extendPos-truePosition)**2).mean(axis=1))).min()


penaltyIndx=(np.where(Errm >1*np.max([xVals[1]-xVals[0],yVals[1]-yVals[0]])))

penaltyPoints=np.squeeze(posTemp.reshape([-1,2])[penaltyIndx,:])


plt.plot(truePosition[:,0],truePosition[:,1], 'k')
plt.plot(penaltyPoints[:,0],penaltyPoints[:,1],'.b')
# for i in range(penaltyPoints.shape[0]):
#     plt.text(penaltyPoints[i,0], penaltyPoints[i,1], str(i))
plt.show()
################



dd_model['P']=np.zeros((nTimeValues,nStateValues))
dd_model['xm']=np.zeros((nTimeValues,2))
dd_model['xs']=np.zeros((nTimeValues,2))
dd_model['Pxx']=np.zeros((nStateValues,nStateValues))

penaltywtsOut=np.ones((nStateValues,))
penaltywtsOut[penaltyIndx]=0.00000001
for i in range(nStateValues):
    if penaltywtsOut[i] <.1:
        m=np.dot(model_params['Ak'],posTemp.reshape([-1,2])[i,:])+model_params['Bk']
        dd_model['Pxx'][i,:]= multivariate_normal.pdf(posTemp, m,model_params['Qk']/4).reshape([-1])
    else:
        dd_model['Pxx'][i, :]=0.00000001

for i in range(nStateValues):

    dd_model['Pxx'][:,i]= dd_model['Pxx'][:,i]/np.sum(dd_model['Pxx'][:,i])

Pxz_prv = dd_model['Pxx'][1,:]
xmu=xPos_te[0]
hpd=0

for ii in range(nTimeValues):
    print ('%d/%d \n'%(ii,nTimeValues))
    dd_model['px']=np.dot(dd_model['Pxx'],Pxz_prv)
    if ii ==0:
        dd_model['P'][ii,:]=dd_model['px']
    else:
        #compute p(xk|zk,hk) using variance of x|z (as computed from GLM)

        Pxz = multivariate_normal.pdf(posTemp, x_mean[ii,:],
                                      np.diag(x_sigma[ii,:])).reshape([-1])

        Pxz = Pxz*penaltywtsOut
        Pxz=Pxz/np.sum(Pxz)
        Pxz_prv = Pxz
        #compute p(xk|z1,...,zk-1) = integral(p(xk|xk-1) * Pprev)
        Pint =np.dot(dd_model['Pxx'],dd_model['P'][ii-1,:].transpose())
        # compute P = p(xk|zk) * p(xk|z1,...,zk-1) / p(xk)
        p=(Pxz *Pint)#/ dd_model['px']
        dd_model['P'][ii, :]=(p)/np.sum(p)
        dd_model['xm'][ii,:]=np.dot(dd_model['P'][ii, :].transpose(),posTemp.reshape([-1,2]))
        dd_model['xs'][ii,:] = np.dot( dd_model['P'][ii, :].transpose(),posTemp.reshape([-1,2])**2)-dd_model['xm'][ii,:]**2

plt.subplot(2,1,1)
plt.plot(t_te,xPos_te,'k')

plt.plot(t_te,x_mean[:,0],'b')
plt.plot(t_te,dd_model['xm'][:,0],':r',alpha=.8)
plt.fill_between(t_te, np.squeeze(dd_model['xm'][:,0]-2*dd_model['xs'][:,0]),
                 np.squeeze(dd_model['xm'][:,0]+2*dd_model['xs'][:,0]), color='g', alpha=.5)

plt.subplot(2,1,2)
plt.plot(t_te,yPos_te,'k')

plt.plot(t_te,x_mean[:,1],'b')
plt.plot(t_te,dd_model['xm'][:,1],':r',alpha=.8)
plt.fill_between(t_te, np.squeeze(dd_model['xm'][:,1]-2*dd_model['xs'][:,1]),
                 np.squeeze(dd_model['xm'][:,1]+2*dd_model['xs'][:,1]), color='g', alpha=.5)

plt.show()