import numpy as np
import scipy.io as sio
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from scipy.linalg import hankel
import tensorflow as tf
import tqdm
import seaborn as sns
import tensorflow_probability as tfp
tfd = tfp.distributions
import warnings
warnings.filterwarnings('ignore')
import math
from keras.layers import Input
from keras.models import Model
from tensorflow.keras import callbacks, optimizers
import statsmodels.api as sm
from scipy.stats import norm
import pandas as pd
from dd_model_utils import *

''' load the dataset
Train and test
idx -> data index
x -> the cognitive state
t -> time of the data
pwords -> 
cwords ->
S -> spiking activi
'''

Data=sio.loadmat('./datasets/Data3_new.mat')

'''preprocess the data; finding informative channels of spikes and
 smoothing them with a gaussian window'''
Eps=np.exp(-19)
dt=.05
dvd=int(1/dt*2)
DataTrain={}
DataTrain['t']=np.linspace(0.0,(np.max(Data['train_t'],axis=-1)),
                           np.int(np.round(np.max(Data['train_t'],axis=-1)/dt) )+1)
Data['train_x']=-Data['train_x']
Data['train_x']=(Data['train_x'] - min(Data['train_x'])) / (max(Data['train_x']) - min(Data['train_x']))
DataTrain['pwords']=Data['train_pwords']
DataTrain['cwords']=Data['train_cwords']
DataTrain['x']=Data['train_x']
AutoEncoderModel={}

AutoEncoderModel['hs']=2
DataTrain['ZRes']=0.05
DataTrain['ZHis']=0.3
DataTrain['bestIdx']=Data['bestIdx']
DataTrain['Ycells']=calSmoothNeuralActivity(np.squeeze(Data['train_S'][:,Data['bestIdx']]),30,4)
DataTrain['SH']=calDesignMatrix(DataTrain['Ycells'],AutoEncoderModel['hs'])
DataTrain['SH']=(DataTrain['SH']-DataTrain['SH'].min())/(DataTrain['SH'].max()-DataTrain['SH'].min())
AutoEncoderModel['MaxItr']=10
AutoEncoderModel['StateSize']=100
AutoEncoderModel['stateBound']=1
xVals = np.linspace(0,
                            AutoEncoderModel['stateBound'],
                            AutoEncoderModel['StateSize'])


####################### BNN ##################

model = tf.keras.Sequential([
    tf.keras.layers.InputLayer(input_shape=[DataTrain['SH'].shape[1]]),

    tfp.layers.DenseVariational(20, posterior_mean_field, prior_trainable, kl_weight=1/DataTrain['SH'].shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :10],
                           scale=1e-4 + tf.math.softplus(.01 * t[...,10:]))),



    tfp.layers.DenseVariational(2, posterior_mean_field, prior_trainable, kl_weight=1/DataTrain['SH'].shape[0]),
    tfp.layers.DistributionLambda(
      lambda t: tfd.Normal(loc=t[..., :1],
                           scale=1e-4 + tf.math.softplus(.01 * t[...,1:]))),

])

# model = tf.keras.Sequential([
#     tf.keras.layers.InputLayer(input_shape=[DataTrain['SH'].shape[1]]),
#
#     tfp.layers.DenseFlipout(40,activation=tf.nn.relu),
#     tfp.layers.DenseFlipout(40,activation=tf.nn.relu),
#     tfp.layers.DenseFlipout(1, tf.nn.sigmoid)
#
#
#
# ])


# num_inducing_points = 40
# model = tf.keras.Sequential([
#     tf.keras.layers.InputLayer(input_shape=[DataTrain['SH'].shape[1]], dtype=DataTrain['SH'].dtype),
#     tfp.layers.DenseVariational(40, posterior_mean_field, prior_trainable, kl_weight=1/DataTrain['SH'].shape[0]),
#     tfp.layers.DistributionLambda(
#       lambda t: tfd.Normal(loc=t[..., :20],
#                            scale=1e-3 + tf.math.softplus(1 * t[...,20:]))),
#     tf.keras.layers.Dense(1, kernel_initializer='ones', use_bias=False),
#     tfp.layers.VariationalGaussianProcess(
#         num_inducing_points=num_inducing_points,
#         kernel_provider=RBFKernelFn(dtype=DataTrain['SH'].dtype),
#         event_shape=[1],
#         inducing_index_points_initializer=tf.constant_initializer(
#             np.linspace(0,1, num=num_inducing_points,
#                         dtype=DataTrain['SH'].dtype)[..., np.newaxis]),
#         unconstrained_observation_noise_variance_initializer=(
#             tf.constant_initializer(
#                 np.log(np.expm1(1.)).astype(DataTrain['SH'].dtype))),
#     ),
# ])
batch_size = np.int(DataTrain['SH'].shape[0]/5)

# kl_weight = 1.0 / 5
# prior_params = {
#     'prior_sigma_1': .1,
#     'prior_sigma_2': 1,
#     'prior_pi': 0.5
# }
#
# x_in = Input(shape=(DataTrain['SH'].shape[1],))
# # x = DenseVariational(10, kl_weight, **prior_params, activation='relu')(x_in)
# x = DenseVariational(10, kl_weight, **prior_params, activation='relu')(x_in)
# x = DenseVariational(1, kl_weight, **prior_params,activation='sigmoid')(x)
#
# model = Model(x_in, x)
# model.summary()

for itr in range(AutoEncoderModel['MaxItr']):
    print('itr %d/%d\n'%( itr, AutoEncoderModel['MaxItr']))
    nTimeValues = DataTrain['t'].shape[0]
    nStateValues = xVals.shape[0]
    AutoEncoderModel['P'] = np.zeros((nTimeValues, nStateValues))
    AutoEncoderModel['Pzx'] = np.zeros((nTimeValues, nStateValues))
    AutoEncoderModel['PSmooth'] = np.zeros((nTimeValues, nStateValues))
    AutoEncoderModel['xkHat'] = np.zeros((nTimeValues, 1))
    AutoEncoderModel['xkHats'] = np.zeros((nTimeValues, 1))
    AutoEncoderModel['xsHat'] = np.zeros((nTimeValues, 1))
    AutoEncoderModel['xsHats'] = np.zeros((nTimeValues, 1))
    AutoEncoderModel['epj'] = np.zeros((nTimeValues, 1))
    AutoEncoderModel['pw'] = np.zeros((nTimeValues, 1))
    AutoEncoderModel['qw'] = np.zeros((nTimeValues, 1))
    AutoEncoderModel['zk'] = np.zeros((nTimeValues, 1))
    AutoEncoderModel['Pxx'] = np.zeros((nStateValues, nStateValues))
    AutoEncoderModel['Pxxc'] = np.zeros((nStateValues, nStateValues))

    if itr == 0:
        Par=parInit(DataTrain,AutoEncoderModel['stateBound'])
        model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.01), loss=negloglik)
        model.fit(DataTrain['SH'], Data['train_x'], batch_size=batch_size, epochs=3000, verbose=0)
        Numb_SampleingTimes = 10
        # Make predictions.
        x_preds = np.zeros([DataTrain['SH'].shape[0], Numb_SampleingTimes])
        yhat = np.zeros([DataTrain['SH'].shape[0], Numb_SampleingTimes])
        for i in range(Numb_SampleingTimes):
            yhat[:, i] = np.squeeze(model.predict(DataTrain['SH']))

        xmu = np.mean(yhat, axis=1)
        Par['XSHs'] = np.std(yhat, axis=1)*np.sqrt(dvd)
        yt = xmu
    else:
        for i in range(nStateValues):
            m = Par['Ak'] * xVals[i] + Par['Bk']
            AutoEncoderModel['Pxx'][i, :] = norm.pdf(xVals, m, Par['sigmaX'])
            AutoEncoderModel['Pxxc'][i, :] = norm.pdf(xVals, m, np.sqrt(dvd)*Par['sigmaX'])

        for i in range(nStateValues):
            AutoEncoderModel['Pxx'][:, i] = AutoEncoderModel['Pxx'][:, i] / np.sum(AutoEncoderModel['Pxx'][:, i])
            AutoEncoderModel['Pxxc'][:, i] = AutoEncoderModel['Pxxc'][:, i] / np.sum(AutoEncoderModel['Pxxc'][:, i])
        temp=AutoEncoderModel['Pxx'][1, :]
        Pxs_prv = np.ones(temp.shape)
        Pxs_prv=Pxs_prv/Pxs_prv.sum()

        ''' Prediction'''
        for ii in range(nTimeValues):
            #print('%d/%d \n' % (ii, nTimeValues))
            AutoEncoderModel['px'] = AutoEncoderModel['Pxx'].dot( Pxs_prv)

            if ii == 0:
                AutoEncoderModel['P'][ii, :] = AutoEncoderModel['Pxxc'].dot( Pxs_prv)
                #print('%f \n' % (AutoEncoderModel['P'][ii, :].sum()))
                #print('%f \n' % (AutoEncoderModel['px'].sum()))

            else:
                '''compute p(zk|xk)'''
                AutoEncoderModel['Pzx'][ii, :], AutoEncoderModel['pw'][ii, 0], AutoEncoderModel['qw'][ii, 0], \
                AutoEncoderModel['zk'][ii, 0]= R_Pzx(DataTrain, Par, xVals, ii)
                if (ii%dvd == 0):
                    #print('%d/%d \n' % (ii, nTimeValues))
                    AutoEncoderModel['P'][ii, :] = AutoEncoderModel['Pxxc'].dot( Pxs_prv)
                    '''predict step compute
                    p(xk | s1, ..., sk - 1) = integral(p(xk | xk - 1) * Pprev)'''
                    Pint = AutoEncoderModel['Pxxc'].dot( AutoEncoderModel['P'][ii - 1, :])
                    '''compute p(zk | xk)'''
                    Pxs = norm.pdf(xVals, xmu[int((ii)/dvd)], (Par['XSHs'][int((ii)/dvd)]))
                    if Pxs.sum() !=0:
                        Pxs=Pxs/Pxs.sum()
                    Pxs_prv = Pxs
                    if AutoEncoderModel['Pzx'][ii,:].sum() !=0:
                        p = (Pxs * Pint * AutoEncoderModel['Pzx'][ii,:])
                    else:
                        p = (Pxs * Pint)
                else:

                    '''predict step compute
                    p(xk | s1, ..., sk - 1) = integral(p(xk | xk - 1) * Pprev)'''
                    Pint = AutoEncoderModel['Pxx'].dot( AutoEncoderModel['P'][ii - 1, :])
                    if AutoEncoderModel['Pzx'][ii, :].sum() != 0:
                        p = ( Pint * AutoEncoderModel['Pzx'][ii,:])
                    else:
                        p = Pint

                if sum(p) !=0:
                    AutoEncoderModel['P'][ii, :] = p / np.sum(p)
                else:

                    AutoEncoderModel['P'][ii, :]=p
            AutoEncoderModel['xkHat'][ii]=xVals.dot(AutoEncoderModel['P'][ii, :])
            AutoEncoderModel['xsHat'][ii] = AutoEncoderModel['P'][ii, :].dot(xVals**2)-AutoEncoderModel['xkHat'][ii]**2
        '''Smoother'''
        for ii in range(nTimeValues):
            dd=nTimeValues-1-ii
            # print('%d/%d \n' % (dd, nTimeValues))
            if dd==nTimeValues-1:
                AutoEncoderModel['PSmooth'][dd, :]=AutoEncoderModel['P'][dd, :]
                AutoEncoderModel['epj'][dd, :] = AutoEncoderModel['xkHat'][dd, :]
            else:
                Pxkp1Sk=AutoEncoderModel['Pxx'].dot(AutoEncoderModel['P'][dd, :])
                temp=AutoEncoderModel['PSmooth'][dd+1, :]/Pxkp1Sk

                AutoEncoderModel['PSmooth'][dd, :]=AutoEncoderModel['P'][dd, :] * \
                                                   AutoEncoderModel['Pxx'].dot(temp)

                Pxxkn1YT=(temp.reshape([-1,1])).dot(Pxkp1Sk.reshape([1, -1]))
                stepErr=((xVals[1:]-xVals[:-1])**2).reshape([1,-1])
                AutoEncoderModel['epj'][dd,0]=(stepErr).dot(np.transpose(stepErr.dot(Pxxkn1YT[:-1,:-1])))

            AutoEncoderModel['xkHats'][dd] = xVals.dot( AutoEncoderModel['PSmooth'][dd, :])
            AutoEncoderModel['xsHats'][dd] = (AutoEncoderModel['PSmooth'][dd, :]).dot(xVals ** 2) - \
                                         AutoEncoderModel['xkHats'][dd] ** 2
        '''system Identification'''
        '''em for pxx'''
        tempSX = scipy.optimize.fminbound(func=ExpectedQ_PXX, x1=0.05,x2=100,
                                          args=(xVals,AutoEncoderModel['PSmooth'],AutoEncoderModel['epj'],dvd))
        if ~math.isnan(tempSX):
            Par['sigmaX']=tempSX
        '''em for pxs'''

        IndxXS0 = range(0, AutoEncoderModel['PSmooth'].shape[0], dvd)
         # Do inference.

        yt=AutoEncoderModel['xkHats'][IndxXS0,:]
        # yt=(yt-yt.min())/(yt.max()-yt.mean())


        #
        #model.compile(optimizer=tf.optimizers.Adam(learning_rate=0.01), loss=neg_log_likelihood)
        model.fit(DataTrain['SH'], yt, batch_size=batch_size, epochs=5000, verbose=0)

        Numb_SampleingTimes = 10
        # Make predictions.
        x_preds = np.zeros([DataTrain['SH'].shape[0], Numb_SampleingTimes])
        yhat = np.zeros([DataTrain['SH'].shape[0], Numb_SampleingTimes])
        for i in range(Numb_SampleingTimes):
            yhat[:, i] = np.squeeze(model.predict(DataTrain['SH']))

        xmu = np.mean(yhat, axis=1)
        Par['XSHs'] = np.std(yhat, axis=1)*np.sqrt(dvd)
        # xmu[np.where(xmu>1) ]=1
        # xmu[np.where(xmu <0)] = 0
        # xmu = (xmu - xmu.min()) / (xmu.max() - xmu.min())
        # if ~math.isnan(np.nansum(tempSHW)):
        #     Par['XSHw']=tempSHW
        '''em for pxz'''
        a0 = np.log(np.nansum(AutoEncoderModel['zk']) / ( Eps+np.nansum(np.exp(Par['a2'] *  AutoEncoderModel['pw'][1:]+\
                                                                          Par['a3']*AutoEncoderModel['qw'][1:])*AutoEncoderModel['PSmooth'][1:,:].dot( np.exp(xVals.reshape([-1,1]))))))
        if ~math.isnan(a0):
            Par['a0']=a0
        #Par['a0']=-5
        # a2a3 = scipy.optimize.fmin(func=ExpectedQ_PXZ, x0=[Par['a2'],Par['a3']]
        #                            , args=(AutoEncoderModel['xkHats'],xVals.reshape([-1,1]),AutoEncoderModel['PSmooth'],
        #                                    AutoEncoderModel['pw'],AutoEncoderModel['qw'],AutoEncoderModel['zk'],
        #                                    Par['a0'],Par['a1']))
        #
        #
        # if ~math.isnan(a2a3[0]):
        #     Par['a2']=a2a3[0]
        #
        # if ~math.isnan(a2a3[1]):
        #     Par['a3']=a2a3[1]
        '''em for px0'''
        temp = scipy.optimize.fmin(func=ExpectedQ_PX0, x0=[Par['muX'], Par['sigmaX']]
                                   , args=(xVals.reshape([-1, 1]), AutoEncoderModel['PSmooth']))


        if ~math.isnan(temp[0]):
            Par['mu0']=temp[0]
        if ~math.isnan( temp[1]):
            Par['sigma0']= temp[1]

    plt.figure()
    plt.subplot(211)
    plt.plot(Data['train_t'].transpose(),
    Data['train_x'] , 'k')
    plt.plot(np.squeeze(Data['train_t']), xmu, 'r')
    plt.plot(np.squeeze(Data['train_t']), yt, 'b')
    plt.fill_between(np.squeeze(Data['train_t']),
                         np.squeeze(xmu) - 2 *np.squeeze( Par['XSHs']/np.sqrt(dvd)),
                         np.squeeze(xmu) + 2 * np.squeeze(Par['XSHs']/np.sqrt(dvd)), color='y', alpha=.5)
    plt.ylim([0,1])
    plt.title(['iteration '+str(itr)])
    plt.subplot(212)
    plt.plot(Data['train_t'].transpose(),Data['train_x'],'k')

    plt.plot(DataTrain['t'],AutoEncoderModel['xkHats'],'r')
    plt.fill_between(np.squeeze(DataTrain['t']), np.squeeze(AutoEncoderModel['xkHats']-2*AutoEncoderModel['xsHats']),
                 np.squeeze(AutoEncoderModel['xkHats']+2*AutoEncoderModel['xsHats']), color='y', alpha=.5)

    yvalsP = 1*AutoEncoderModel['stateBound']*np.ones([DataTrain['pwords'].shape[0],])
    yvalsc = 0*AutoEncoderModel['stateBound']*np.ones([DataTrain['cwords'].shape[0],])
    plt.scatter(np.squeeze(DataTrain['pwords']),yvalsP)
    plt.scatter(np.squeeze(DataTrain['cwords']),yvalsc)
    plt.show()
plt.figure()
plt.subplot(3,1,1)
plt.plot(np.squeeze(DataTrain['t']),AutoEncoderModel['pw'],'r.')
plt.ylabel('pw')
plt.subplot(3,1,2)
plt.plot(np.squeeze(DataTrain['t']),AutoEncoderModel['qw'],'r.')
plt.ylabel('qw')
plt.subplot(3,1,3)
plt.plot(np.squeeze(DataTrain['t']),AutoEncoderModel['zk'],'r.')
plt.ylabel('zk')
plt.show()