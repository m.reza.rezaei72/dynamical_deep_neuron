import numpy as np
import scipy.io as sio
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from scipy.linalg import hankel
from scipy.stats import multivariate_normal
import tensorflow as tf
import tqdm
import seaborn as sns
import tensorflow_probability as tfp
tfd = tfp.distributions
import warnings
warnings.filterwarnings('ignore')
from keras.layers import Input
from keras.models import Model
from tensorflow.keras import callbacks, optimizers
import statsmodels.api as sm
from scipy.stats import norm
import pandas as pd
from dd_model_utils import *

''' load the dataset
data spec: MSTrain:27305x67 (time resolution 33ms)
col[0] : time index
col[1-62] : spiking activities
col[63] : x-position
col[64] : y-position
col[65] : x-velocity
col[66] : y-velocity
'''

Data=sio.loadmat('./datasets/Data33.mat')['MSTrain']

'''preprocess the data; finding informative channels of spikes and
 smoothing them with a gaussian window'''


NeuralData=Data[:,1:63]
bestIndxs=calInformetiveChan(NeuralData,100)




YCells=calSmoothNeuralActivity(np.squeeze(NeuralData[:,bestIndxs]),100,20)
Data[:,63]=(Data[:,63]-np.min(Data[:,63]))/(np.max(Data[:,63])-np.min(Data[:,63]))
Data[:,64]=(Data[:,64]-np.min(Data[:,64]))/(np.max(Data[:,64])-np.min(Data[:,64]))
(YCells_tr,YCells_te,xPos_tr,xPos_te,yPos_tr,yPos_te,t_tr,t_te)=train_test_split(YCells, Data[:,63],Data[:,64],Data[:,0], test_size=.2,random_state=0,shuffle=False) # split test and train daata

#plt.subplot(2,1,1)
#plt.plot(xPos_tr)
#plt.subplot(2, 1, 2)
#plt.plot(yPos_tr)
#plt.show()

model_params = {'Ak': np.eye(2),
                'Bk': np.array([0,0]).transpose(),
                'Qk': np.diag([np.std(xPos_tr),np.std(yPos_tr)])/4,
                'sxy':np.eye(2),
                'history_term':np.array([10,10]).transpose(),
                'Max_step':20,
                'std_xy':np.array([np.std(xPos_tr),np.std(yPos_tr)]).transpose(),
                'mean_xy':np.array([np.mean(xPos_tr),np.mean(yPos_tr)]).transpose()}

dd_model={'model_params':model_params,
          }

################### GLM Y ###################
XSgn_y_tr=calDesignMatrix(YCells_tr,model_params['history_term'][1])
XSgn_y_te=calDesignMatrix(YCells_te,model_params['history_term'][1])
model_coefficients_start_y = tf.zeros(XSgn_y_tr.shape[-1], np.float64)
model_coefficients_y, linear_response_y_tr, is_converged_y, num_iter_y = tfp.glm.fit(
   model_matrix=tf.convert_to_tensor(XSgn_y_tr),
   response=tf.convert_to_tensor(yPos_tr),
   model=tfp.glm.Normal(),
   model_coefficients_start=model_coefficients_start_y)
GLM_model_y = sm.GLM(yPos_tr,XSgn_y_tr , family=sm.families.Gaussian())
GLM_results_y = GLM_model_y.fit()
#print(GLM_results_y.summary())
linear_response_y_te=np.dot(XSgn_y_te,GLM_results_y.params)
linear_response_y_tr=np.dot(XSgn_y_tr,GLM_results_y.params)
################### GLM X ###################
XSgn_x_tr=calDesignMatrix(np.concatenate([YCells_tr,linear_response_y_tr.reshape([-1,1])],axis=-1),
                          model_params['history_term'][0])
XSgn_x_te=calDesignMatrix(np.concatenate([YCells_te,linear_response_y_te.reshape([-1,1])],axis=-1),
                          model_params['history_term'][0])
model_coefficients_start_x = tf.zeros(XSgn_x_tr.shape[-1], np.float64)
model_coefficients_x, linear_response_x_tr, is_converged_x, num_iter_x = tfp.glm.fit(
   model_matrix=tf.convert_to_tensor(XSgn_x_tr),
   response=tf.convert_to_tensor(xPos_tr),
   model=tfp.glm.Normal(),
   model_coefficients_start=model_coefficients_start_x)
GLM_model_x = sm.GLM(xPos_tr,XSgn_x_tr , family=sm.families.Gaussian())
GLM_results_x = GLM_model_x.fit()
#print(GLM_results_x.summary())
linear_response_x_te=np.dot(XSgn_x_te,GLM_results_x.params)
plt.plot(xPos_te,yPos_te,'k')

plt.plot(linear_response_x_te,linear_response_y_te,'b')
plt.show()
##################

xVals=np.linspace(model_params['mean_xy'][0]-4*model_params['std_xy'][0],
                  model_params['mean_xy'][0]+4*model_params['std_xy'][0],
                  model_params['Max_step'])

yVals=np.linspace(model_params['mean_xy'][1]-4*model_params['std_xy'][1],
                  model_params['mean_xy'][1]+4*model_params['std_xy'][1],
                  model_params['Max_step'])

[xValM,yValM]=np.meshgrid(xVals,yVals)
posTemp = np.dstack((xValM,yValM))
MVN=multivariate_normal( model_params['mean_xy'],model_params['Qk'])
dd_model['px']=MVN.pdf(posTemp)
#fig2 = plt.figure()
#ax2 = fig2.add_subplot(111)
#ax2.contourf(xValM, yValM, dd_model['px'])
#plt.show()
dd_model['px']=np.stack(dd_model['px'])



nTimeValues= xPos_te.shape[0]
nStateValues = xVals.shape[0]*yVals.shape[0]

dd_model['P']=np.zeros((nTimeValues,nStateValues))
dd_model['xm']=np.zeros((nTimeValues,2))
dd_model['xs']=np.zeros((nTimeValues,2))
dd_model['Pxx']=np.zeros((nStateValues,nStateValues))

for i in range(nStateValues):
    m=np.dot(model_params['Ak'],[xValM.reshape([-1])[i],yValM.reshape([-1])[i]])+model_params['Bk']
    dd_model['Pxx'][i,:]= multivariate_normal.pdf(posTemp, m,model_params['Qk']).reshape([-1])

for i in range(nStateValues):

    dd_model['Pxx'][:,i]= dd_model['Pxx'][:,i]/np.sum(dd_model['Pxx'][:,i])

Pxz_prv = dd_model['Pxx'][1,:]
xmu=xPos_te[0]
hpd=0

for ii in range(nTimeValues):
    print ('%d/%d \n'%(ii,nTimeValues))
    dd_model['px']=np.dot(dd_model['Pxx'],Pxz_prv)
    if ii ==0:
        dd_model['P'][ii,:]=dd_model['px']
    else:
        #compute p(xk|zk,hk) using variance of x|z (as computed from GLM)

        Pxz = multivariate_normal.pdf(posTemp, [linear_response_x_te[ii],
                                                linear_response_y_te[ii]], model_params['Qk']).reshape([-1])
        Pxz_prv = Pxz
        #compute p(xk|z1,...,zk-1) = integral(p(xk|xk-1) * Pprev)
        Pint =np.dot(dd_model['Pxx'],dd_model['P'][ii-1,:].transpose())
        # compute P = p(xk|zk) * p(xk|z1,...,zk-1) / p(xk)
        p=(Pxz *Pint)/ dd_model['px']
        dd_model['P'][ii, :]=p/np.sum(p)
        dd_model['xm'][ii,:]=np.dot(dd_model['P'][ii, :].transpose(),posTemp.reshape([-1,2]))
        dd_model['xs'][ii,:] = np.dot( dd_model['P'][ii, :].transpose(),posTemp.reshape([-1,2])**2)-dd_model['xm'][ii,:]**2

plt.subplot(2,1,1)
plt.plot(t_te,xPos_te,'k')

plt.plot(t_te,linear_response_x_te,'b')
plt.plot(t_te,dd_model['xm'][:,0],':r',alpha=.8)
plt.fill_between(t_te, np.squeeze(dd_model['xm'][:,0]-2*dd_model['xs'][:,0]),
                 np.squeeze(dd_model['xm'][:,0]+2*dd_model['xs'][:,0]), color='g', alpha=.5)

plt.subplot(2,1,2)
plt.plot(t_te,yPos_te,'k')

plt.plot(t_te,linear_response_y_te,'b')
plt.plot(t_te,dd_model['xm'][:,1],':r',alpha=.8)
plt.fill_between(t_te, np.squeeze(dd_model['xm'][:,1]-2*dd_model['xs'][:,1]),
                 np.squeeze(dd_model['xm'][:,1]+2*dd_model['xs'][:,1]), color='g', alpha=.5)

plt.show()